// On crée la fonction qui désactive l'affichage des "tooltips"

function disableTooltips(){
    
    var tooltips = document.querySelectorAll('.tooltip'),
        tooltipsLength = tooltips.length;
    
    for(var i = 0; i < tooltipsLength; i++ ){
        tooltips[i].style.display = 'none';
    }
}

// La fonction ci-dessous permet la récupération de donnés dans l'input 

function getTooltip(elements) {
    while(elements = elements.nextSibling){
        if(elements.className === 'tooltip') {
            return elements;
        }
    }
    return false;
}

// Fonctions qui check le formulaire , si ok > TRUE else FALSE

var check = {}; // On met toutes nos fonctions dans un objet littéral 

check['sex'] = function(){
    
    var sex = document.getElementById('sex'),
        tooltipStyle = getTooltip(sex[1].parentNode).style;
    
    if (sex[0].checked || sex[1].checked) {
        tooltipStyle.display = 'none';
        return true;
    }
    else{
        tooltipStyle.display = 'inline-block';
        return false;
    }
}