<?php

/* core/modules/forum/templates/forum-icon.html.twig */
class __TwigTemplate_bae943fb56a48af2ea9d50793f2175224e5fb4cde4b0960be7c6bd525670c702 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 22);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 21
        echo "<div";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["attributes"]) ? $context["attributes"] : null), "html", null, true));
        echo ">
  ";
        // line 22
        if ((isset($context["first_new"]) ? $context["first_new"] : null)) {
            // line 23
            echo "<a id=\"new\"></a>";
        }
        // line 25
        echo "  <span class=\"visually-hidden\">";
        echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, (isset($context["icon_title"]) ? $context["icon_title"] : null), "html", null, true));
        echo "</span>
</div>
";
    }

    public function getTemplateName()
    {
        return "core/modules/forum/templates/forum-icon.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 25,  50 => 23,  48 => 22,  43 => 21,);
    }

    public function getSource()
    {
        return "{#
/**
 * @file
 * Default theme implementation to display a status icon for a forum post.
 *
 * Available variables:
 * - attributes: HTML attributes to be applied to the wrapper element.
 *   - class: HTML classes that determine which icon to display. May be one of
 *     'hot', 'hot-new', 'new', 'default', 'closed', or 'sticky'.
 *   - title: Text alternative for the forum icon.
 * - icon_title: Text alternative for the forum icon, same as above.
 * - new_posts: '1' when this topic contains new posts, otherwise '0'.
 * - first_new: '1' when this is the first topic with new posts, otherwise '0'.
 * - icon_status: Indicates which status icon should be used.
 *
 * @see template_preprocess_forum_icon()
 *
 * @ingroup themeable
 */
#}
<div{{ attributes }}>
  {% if first_new -%}
    <a id=\"new\"></a>
  {%- endif %}
  <span class=\"visually-hidden\">{{ icon_title }}</span>
</div>
";
    }
}
