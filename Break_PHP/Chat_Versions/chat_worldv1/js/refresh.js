$(document).ready(function() {
	/*
	* $.ajax() = on instancie un objet "XHR" avec jquery
	* Function get : Elle récupere les données recus en GET 
	* Function send: Elle envoi les données user & messages depuis l'id form et qui prend les input ligne par ligne
	* Function pour annuler la méthode d'envoi par défaut du formulaire
	*/
	var add = function(){
		$.ajax({
			url: 'ajax.php',
			method: 'GET',
			data: $("#form").serialize()
		});
	};

	$('#form').submit(function(e){ 
		e.preventDefault();
		add();
	});	
});

function lireMess(){
		$.ajax({
			method:"GET",
			url:"ajax.php",                // JE T RAJOUTER CETTE FUNCTION POUR LIRE LES PSEUDO ET MESSAGE VIA AJAX !!
			data:"read"
		}).done(function(data){
			$("#tchat").html(data);
		});
	};		

setInterval(lireMess,1000); // J'EXECUTE LA FONCTION AVEC UN INTERVALE DE 1s