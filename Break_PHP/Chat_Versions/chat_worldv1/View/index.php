<?php 
/*$contr = new ChatContr();*/
/*if (isset($_GET['user']) AND isset($_GET['message'])) {
	$contr->addMessage($_GET['user'],$_GET['message']);
}*/
?><!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Chat_Mvc</title>
		<link rel="stylesheet" type="text/css" href="../css/styles.css">
		<script type="text/javascript" src="../js/jquery.js"></script>
		<style type="text/css">

		</style>
	</head>
	<body>
		<header> Menu 
			<div id="div1"><h2>Membres</h2></div>

				<button id="online">Voir qui est en ligne</button>
		</header>
		<div class="wrapper">
			<article>
				<h1>Chat_World</h1>
				<form id="form">
					<input type="text" name="user" placeholder="Votre pseudo">
					<input type="text" name="message" placeholder="Message">
					<input type="submit" id="tchater" value="Tchatter">
				</form>
				<article>
					<div id="tchat" style="overflow: scroll; height: 200%;"></div>
				</article>
				<aside> <!-- Membres en ligne -->
					<div class="heure">
						<p>Aujourd'hui nous sommes le <?php echo date('d/m/Y h:i:s'); ?>.</p>
					</div>
				</aside>
			</article>

			<!-- JavaScript -->
			<script type="text/javascript" src="../js/refresh.js"></script>
			<script>
				$(document).ready(function(){
			    $("button").click(function(){
			        $("#div1").load("test.php");
			        	$('#online').fadeToggle('500');
			    });
			});

		</script>
			
  		</div> <!-- /wrapper -->
		<footer>Copy by Walid Belbeche</footer>
	</body>
</html>