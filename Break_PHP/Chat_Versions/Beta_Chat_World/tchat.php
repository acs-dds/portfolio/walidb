<?php
/*
*Je crée une function qui vérifier si une valeur à bien été recus dans pseudo
* sinon index.php
*
*/
session_start();
if(!isset($_SESSION["pseudo"]) || empty($_SESSION["pseudo"])) {
	header('Location: index.php');
}
include 'connect.php';
?>

<!DOCTYPE html>
<html>
<head>
	<title>Chat_World_V0</title>
	<h1 class="titre">|C|W| Chat_World |C|W|</h1>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="tchat.js"></script>
	<script type="text/javascript">
		<?php
			$sql = "SELECT id FROM message ORDER BY id DESC LIMIT 1";
		 	$req = $bdd->query($sql);
		 	$data= $req->fetch(PDO::FETCH_ASSOC);
		?>
		var lastid = <?php echo $data['id'];?>
	</script>

</head>
<body>

	<div>
		<h1>Connecter en tant que <?php echo $_SESSION["pseudo"]; ?>"</h1>
			<div id="chat">
				<?php
					include('connect.php'); 
					/*$bdd->exec("INSERT INTO `message` (`id`, `pseudo`, `message`, `date`) VALUES ('pseudo','message',".time().")");*/
										
					$sql = $bdd->query("SELECT * FROM message ORDER BY date DESC LIMIT 15");
					$d = array();
					$sql->setFetchMode(PDO::FETCH_ASSOC); 
					// Tableau assosiatif 
					while($data = $sql->fetch()){
						$d[] = $data;
					}
					for($i=count($d)-1;$i>=0;$i--){
					?>
						<p><strong><?php echo $d[$i]['pseudo']; ?></strong> : <?php echo htmlentities($d[$i]['message']); ?> </p>
					<?php
					}
				?>
			</div>
		<div id="tchatForm" style="position: fixed;bottom:0;width: 100%;">
			<form method="post" action="#">
				<div style="margin-right:110px;">
					<textarea class="tchat" name="message"></textarea>
					<input type="submit" value="reponse">
				</div>
				<div style="position: absolute;top:12px;right: 0;">
				</div>
			</form>
		  </div>
	</div>
</body>
</html>