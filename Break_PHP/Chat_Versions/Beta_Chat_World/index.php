<?php


if(!empty($_POST) && isset($_POST['pseudo']) && !empty($_POST['pseudo']))
{
	session_start();
	$_SESSION["pseudo"] = $_POST['pseudo'];
	header('Location: tchat.php');
}
?>

<!DOCTYPE html>
<html>
<head>
	<title>Mon chat</title>
	<style type="text/css">
		body {
			background-image: url("image/bg.jpg");
			text-align: center;
		}
		.panel_gauche {
			width: 100%;
			float: left;
		}
		.panel_gauche, input {
			position: relative;
		}
		.panel_gauche:hover input {
			margin-right: 30%;
			transition: 1.5s;
		}
	</style>
	<link rel="stylesheet" type="text/css" href="styles.css">
	<script type="text/javascript" src="jquery.js"></script>
	<script type="text/javascript" src="tchat.js"></script>
</head>
<body>

	<div>
		<h1>ChatWorld</h1>
		<form action="index.php" method="post">
			Invité : <input type="text" name="pseudo"/>
			<input type="submit" value="tchatter"/>
		</form>
		<br/>
		<div class="panel_gauche">
			<input type="submit" name="inscription" value="inscription"/>
			<input type="submit" name="connexion" value="Connexion"/>
		</div>
	</div>

</body>
</html>