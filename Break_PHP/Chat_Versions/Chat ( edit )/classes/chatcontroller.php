<?php

require __DIR__.'/messagemapper.php';

class ChatController {
	private $mapper;
	
	public function __construct() {
		session_start();
		setlocale(LC_TIME, 'fr_FR');
		if (isset($_SESSION['discussion'])) {
			$this->mapper = new MessageMapper($_SESSION['discussion']);
		} else {
			$this->mapper = new MessageMapper();
		}
	}

	public function setDiscussion($channel) {
		$_SESSION['discussion'] = $channel;
		return 0;
	}

	private function isRegistered() {
		return isset($_SESSION['nom']);
	}

	public function getHistory() {
		if (!$this->isRegistered()) return 2;
		$messages = $this->mapper->getMessages();
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>".$message->renderHtml()."</li>";
		}
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

	public function getNewMessages() {
		if (!$this->isRegistered()) return 2;
		// si un utilisateur appelle cette fonction avant getHistory(), $_SESSION['last_update'] ne sera pas défini => bug
		// du coup, pour ce cas-là, on "redirige" vers getHistory()
		if (!isset($_SESSION['last_update'])) return $this->getHistory();
		$messages = $this->mapper->getMessages($_SESSION['last_update']);
		$html = "";
		foreach ($messages as $message) {
			$html .= "<li>".$message->renderHtml()."</li>";
		}
		$_SESSION['last_update'] = microtime(true);
		return $html;
	}

	public function register($nom) {
		$_SESSION['nom'] = $nom;
		return 0;
	}

	public function postMessage($message) {
		if (!$this->isRegistered()) return 2;
		$this->mapper->addMessage(
			$obj = new Message($_SESSION['nom'], microtime(true), $message)
		);
		return $this->getNewMessages();
	}
}