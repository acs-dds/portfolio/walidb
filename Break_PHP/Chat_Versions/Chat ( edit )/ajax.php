<?php

require "classes/chatcontroller.php";

if (count($_GET) > 0) {
	$c = new ChatController();
	if (isset($_GET['nom'])) {
		echo $c->register($_GET['nom']);
	}
	if (isset($_GET['message'])) {
		echo $c->postMessage($_GET['message']);
	}
	if (isset($_GET['channel'])) {
		echo $c->setDiscussion($_GET['channel']);
	}
	if (isset($_GET['get'])) {
		if (isset($_GET['new'])) {
			echo $c->getNewMessages();
		} else {
			echo $c->getHistory();
		}
	}
}