<!DOCTYPE html>
<html lang="fr">
  <head>
  <link rel="stylesheet" type="text/css" href="data/styles.css">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- <link rel="icon" href="../../favicon.ico"> -->

    <title>LookThis</title>

    <!-- Bootstrap core CSS -->
    <link href="data/app.css" rel="stylesheet">

  </head>

  <body>

    <nav class="navbar navbar-default" class="responsive">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">LookThis</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="active"><a href="register.php">S'inscrire</a></li>
            <li><a href="about.php">Qu'elle avantages ?</a></li>
            <li><a href="release.php">Réalisations</a></li>
            <li><a href="contact.php">Contactez-Moi</a></li>
          </ul>
        </div>
        <form class="navbar-form navbar-left" role="search">
        <div class="form-group">
            <input type="text" class="form-control" placeholder="Que recherchez vous?">
        </div>
        <button type="submit" class="btn btn-default">Go</button>
      </form>
      </div>
    </nav>

    <div class="container">


