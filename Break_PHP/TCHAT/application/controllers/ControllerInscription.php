<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ControllerInscription extends CI_Controller{
	public function __construct(){
		parent::__construct();

		$this->load->model("Inscription_Model");
		$this->load->helper(array('form','url'));
		$this->load->library('form_validation');

		$this->output->enable_profiler(true);
	}

	public function index(){
		$this->form_validation->set_rules("pseudo","Pseudo","trim|required");
		$this->form_validation->set_rules("mail","Mail","trim|valid_email|required");
		$this->form_validation->set_rules("pass","Pass","trim|required");
		$this->form_validation->set_rules("passconfirm","Passconfirm","trim|required|matches[pass]");

		if($this->form_validation->run() != false){
			$this->load->view('myform');
		}
		else{
			$pseudo = $this->input->post('pseudo');
			$mail = $this->input->post('mail');
			$pass = $this->input->post('pass');
			$this->load->view('formsuccess');

				/*($this->Inscription_Model->register($pseudo,$mail,$pass))*/
		}
	}
}