<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Tchat | Inscription</title>
		<link rel="stylesheet" type="text/css" href="static/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="static/css/register.css">
	</head>

	<body>
		<div class="bloc_register">
			<?= form_open() ?>
				<label class="col-md-4 control-label">Pseudo</label>
				<input id="pseudo" type="text" name="pseudo"><br>

				<label class="col-md-4 control-label">Adresse mail</label>
				<input id="mail" type="mail" name="mail"><br>

				<label class="col-md-4 control-label">Mot de passe</label>
				<input id="pass" type="password" name="pass"><br>

				<label class="col-md-4 control-label">Confirmer mot de passe</label>
				<input id="pass" type="password" name="passconfirm"><br>

				<input id="send" type="submit" value="Confirmer">
			</form>
		</div>
	</body>
</html>