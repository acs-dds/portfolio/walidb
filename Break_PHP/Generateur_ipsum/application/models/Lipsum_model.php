<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lipsum_model extends CI_Model {

        private $base;

        private $themes = [
                'zombies' => ['Zombies','Cerveau','Démembrer','Ramper','Tripes','Sang','Gore'],
                'glace' => ['Macadamia Nut Brittle','Vanille Pecan','Pralines & Cream','Cookies & Cream','Strawberry Cheesecake','Chocolate Midnight Cookies'],
                'metal' => ['Leprous','Immortal','Iron Maiden','Slayer','Satan\'s Penguins','Behemoth','Borknagar','Tyr','Architects']
        ];

        public function __construct() {
                parent::__construct();
                $this->base = ["maecenas", "sit", "amet", "aliquam", "nunc", "ac", "pellentesque", "nisl", "nunc", "sapien", "ligula", "pellentesque", "quis", "cursus", "eget", "finibus", "et", "quam", "morbi", "ornare", "ullamcorper", "convallis", "ut", "at", "leo", "varius", "pretium", "felis", "eget", "tempus", "arcu", "aenean", "aliquet", "lorem", "quis", "auctor", "egestas", "nam", "vitae", "ipsum", "a", "lectus", "pretium", "venenatis", "pulvinar", "eu", "est", "morbi", "ullamcorper", "nibh", "sit", "amet", "enim", "sagittis", "at", "volutpat", "ligula", "placerat", "duis", "sem", "augue", "tempor", "sed", "massa", "cursus", "aliquam", "blandit", "nisi", "sed", "eleifend", "tincidunt", "dui", "quis", "laoreet", "massa", "efficitur", "et", "sed", "dui", "sapien", "volutpat", "et", "finibus", "nec", "ullamcorper", "suscipit", "mauris", "cras", "ultrices", "ex", "et", "nulla", "congue", "tempus", "curabitur", "finibus", "commodo", "tristique", "nullam", "pharetra", "ex", "at", "purus", "sodales", "non", "faucibus", "neque", "scelerisque"];
        }

        public function loadTheme($nom) {
                if (isset($this->themes[$nom])) {
                        $this->base = array_merge($this->base, $this->themes[$nom]);
                } else {
                        echo 'Thème '.$nom.' inexistant';
                }
        }

        public function genererParagraphes($nombre) {
                $ret = [];

                for ($i=0; $i < $nombre; $i++) { 
                        $ret[] = $this->genererParagraphe();
                }

                $ret = implode(PHP_EOL.PHP_EOL, $ret);

                return $ret;
        }

        public function genererParagraphe() {
                

                $longueurBase = count($this->base);

                $longueurParagraphe = rand(50, 150);

                $ret = [];
                for ($i=0; $i < 100; $i++) { 
                        $indice = rand(0, $longueurBase - 1);
                        $ret[] = $this->base[$indice];
                }

                $ret = implode(" ", $ret);

                return "<p>$ret</p>";
        }

}