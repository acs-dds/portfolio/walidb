<?php include '2501.php'; ?>
<?php
require __DIR__.'../2401.php';
// j'étudie le fichier inclus : il contient un array associatif au format intitulé => url avec un sous-menu pour un des intitulés

// je crée mon menu mère
echo '<ul class="menu">';
foreach ($menu as $item => $url) { // je boucle sur le menu
	if (is_array($url)) { // s'il y a un sous-menu
		echo '<li>'.$item.'<ul class="submenu">'; // je commence à créer le li avec l'intitulé et l'ul du sous-menu
		foreach ($url as $subitem => $suburl) { // je boucle sur le sous-menu
			echo '<li><a href="'.$suburl.'">'.$subitem.'</a></li>'; // j'affiche un li contenant un lien dont l'intitulé est celui trouvé dans le $menu et la cible l'url trouvée dans ce même $menu
		}
		echo '</ul></li>'; // je ferme mon sous-menu et mon li
	} else { // si c'est un simple item de menu
		echo '<li><a href="'.$url.'">'.$item.'</a></li>'; // j'affiche un li contenant le lien avec l'intitulé et l'url correspondant
	}
}
echo '</ul>'; // et je referme mon menu mère après la boucle
?>