<?php


/**
* Model Class Form_Register
*/
class Register_Model extends CI_Model
{
	public function __construct()
	{
		parent::__construct();
		$this->load->database();
	}
	
	function login($username,$password){
		$user = $this->db->where(array('username' => $username, 'password' => sha1($password)))->get('login');
		if($user->num_rows() != 0){
			return $user->first_row('array');
		} 
		else 
		{
			return false;
		}
	}
}