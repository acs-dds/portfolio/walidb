<?php

/*
*Model class Code igniter 
* Function : Register => 3 args , + 1 variable qui contient un tableau des 3 entité et stoquer dans les ARGS
* Instruction function : Qui met le f
*
* 
*/
 

class UserModel extends CI_Model
{

    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

	public function register($username,$mdp,$email){
			$data = array(
				'pseudo' => $username,
				'mdp' => $mdp,
				'email' => $email
			);
			return $this->db->insert('register', $data);
			echo 'champs incorrects';
/*		$this->load->view('includes/header');
		$this->load->view('Signup_view');
		$this->load->view('includes/footer');*/
	}

	public function login($username,$password){
		$this->db->select("pseudo,mdp");
		$this->db->from("register");
		$this->db->where("pseudo",$username);
		$this->db->where("mdp",$password);

		return $this->db->get()->result();
	}

	public function get_message($message,$user_id){

		$data = array(
		'message' => $message,
		'utilisateur_id' => $user_id
		);

		$this->db->insert('message', $data);
	}
	public function read_msg(){
		$this->db->select('message,temps_message,utilisateur_id,salon_id,register.pseudo');
		$this->db->from("message");
		$this->db->join('register','message.utilisateur_id = register.id');
		//$this->db->order_by('message');
		foreach ($this->db->get()->result() as $msg) {
			echo '<b>'.$msg->pseudo.'</b> : '.$msg->message.'<hr>';
			// var_dump($msg);
		}
	}

	public function get_id($username)
	{
		$query = $this->db->query("SELECT id,pseudo FROM register WHERE pseudo = ?",array($username));
		foreach ($query->result() as $msg) {
			return $msg->id;
		}
	}
	public function disconnect(){
		echo '<button>'.'</button>';
		$this->session->unset();
	}

	public function insert_msg($message,$channel,$temps_message,$user_id,$salon_id){

		$data_msg = array(
		'message' => $message,
		'temps_message' => $temps_message,
		'utilisateur_id' => $user_id,
		'salon_id' => $salon_id
		);
		$this->db->insert('message',$data_msg);
	}

	public function get_username($user_id)
	{
		$query = $this->db->query("SELECT pseudo FROM register WHERE id = ?",array($user_id));
		foreach ($query->result() as $msg) {
			return $msg->pseudo;
		}
	}
}