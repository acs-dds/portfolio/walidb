<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>|-C.W| Page d'inscription </title>
		<link rel="stylesheet" type="text/css" href="\code_igniter_tchat\assets\css\bootstrap.min.css">
		<script type="text/javascript" src="\code_igniter_tchat\assets\js\jquery-3.1.1.min.js"></script>
		<script type="text/javascript" src="\code_igniter_tchat\assets\js\bootstrap.js"></script>
	</head>
<body>

<div class="container">
<div class="row">
	<center>
	<form action="" method="post">
		<h1>Chat_World</h1>
		<h3>Veuillez vous inscrire afin d'acceder au Tchat</h3>
		<div class="row">
				<h4>User Register Form</h4>
		</div>
		<div class="panel-body">
			<div class="form-group">
			<label for="name">Username</label>
			<input class="form-control" name="fname" placeholder="Username" type="text"/>
			<span class="text-danger"><?= form_error('fname');?></span>
			</div>
		</div>

        <div class="panel-body">
        <div class="form-group">
        	<label for="subjet">Password</label>
        	<input class="form-control" name="password" placeholder="Password" type="password">
        	<span class="text-danger"><?= form_error('password');?></span>
        </div>
        </div>

        <div class="panel-body">
        <div class="form-group">
            <label for="email">Email</label>
            <input class="form-control" name="email" placeholder="Email-ID" type="text" />
            <span class="text-danger"><?= form_error('email'); ?></span>
        </div>
        </div>

        <div class="form-group">
        	<button name="submit" type="submit" class="btn btn-default">Inscription</button>
        	<button name="cancel" type="reset" class="btn btn-default">Cancel</button>
        </div>
        </form>
	</center>
</div>
</div>
</body>
</html>