<?php 


/**
* Controller Login 
*/
class Login extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(true);
		$this->load->model('LoginModel');

	}

	public function index($username,$password){
		
		if ($username === $password){
			$this->load->view('Login_view');
			//redirect('Login/index');
		}
		else
		{
			echo "Veuillez remplir les champs s'il vous plais.";
			redirect('Home_view');
		}
		
		
	}
}