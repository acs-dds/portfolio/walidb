<?php 

/**
* Register Controller : 
* __Construct(), construit la class de base
* On load la library , le helper URL et FORM et le model userModel
*Function REG : qui récupere les informations en POST
* Function: Login qui met les regles au POST plus haut et on met le formulaire à faux par défaut
* 
*/
class Home extends CI_Controller
{
	
	public function __construct(){
		parent::__construct();

		$this->load->library('form_validation');
		$this->load->helper(array('url','form'));
		$this->load->model('UserModel');

		$this->output->enable_profiler(true);
	}

	public function reg(){
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		$this->form_validation->set_rules('email','email','required');

		if ($this->form_validation->run() == FALSE){
			$this->load->view('includes/header');
			$this->load->view('Signup_view');
			$this->load->view('includes/footer');
		}
		else // En attente d'informations
		{
			echo '<center>'.'Votre inscription as bien été prise en compte.'.'</center>';

			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');
				// On récupere les informations enregistré
			$this->UserModel->register($username,$password,$email);
			redirect('Home/login');
		}
	}

	public function login(){
		$this->form_validation->set_rules('username','username','required');
		$this->form_validation->set_rules('password','password','required');
		// Si il y'a une demande à envoyer
		if($this->form_validation->run() == FALSE){
			$this->load->view('includes/header');
			$this->load->view('Login_view');
			$this->load->view('includes/footer');
		}
		else{
			// Sinon on demande l'informations
			$username = $this->input->post('username');
			$password = $this->input->post('password');

			// Si les informations corespondent au variables , alors redirection Home/room(Tchat)
			if($this->UserModel->login($username,$password))
			{
				$user_id = $this->UserModel->get_id($username);

			$newdata = array(
			    'user_id'  => $user_id,
			    'logged_in' => TRUE
			);

			$this->session->set_userdata($newdata);
				redirect('Home/room');
			}
			else 
			{
				echo "<center>"."<div>".'Des champs ont été mal renseignez , merci de reformuler vos saisies'."</div>"."</center>";
			}				
		}		
	}

	public function room(){

		$this->form_validation->set_rules('message','message','required');
		if ($this->form_validation->run() == FALSE){
			/*$this->UserModel->read_msg();*/ // old show
			$this->load->view('includes/header');
			$this->load->view('Home_view');
			$this->load->view('includes/footer');
		}
		else
		{
			$message = $this->input->post('message');
			$this->UserModel->get_message($message,$this->session->user_id);	
		}
	}

	public function insert_msg(){

		$message = $this->input->post("message");
		$channel = $this->input->post("discussion");
		$login = $this->UserModel->get_username($this->session->user_id);
		var_dump($login);

		$this->UserModel->insert_msg;
	}

	public function disconnect(){
		$this->session->unset();
	}

}