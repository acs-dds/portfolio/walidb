<?php 

/**
* Signup _ Controller
*/
class signup extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
		/*$this->load->helper(array('form','url'));*/
		/*$this->load->library(array('form_validation'));*/
		$this->load->model('User_model');
	}

	function register($fname,$password,$email)
	{
		
		$this->User_model->register($fname,$password,$email);

		$fname = $this->input->post('fname');
		$password = $this->input->post('password');
		$email = $this->input->post('email');

		$this->load->view('Signup_view');
		return $this->db->get()->result();
	}
}