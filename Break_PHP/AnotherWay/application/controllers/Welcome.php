<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->output->enable_profiler(TRUE);
		$this->load->model('Model');
	}

	/**
	 * Default controller
	 *
	 */
	public function index()
	{
		$this->load->view('Portail');
	}

	public function Login($login,$pass,$stats){
		
		$data['user'] = array(
		'user' => $login,
		'pass' => $pass,
		'status' => $stats
		);

		$login = $this->input->post('user');
		$pass = $this->input->post('pass');

		if (isset($login) AND isset($pass)){
			return $this->Model->login($login,$pass);
			//$this->load->view('Login',$data);
		}
		$this->load->view('Login',$data);
	}
}
