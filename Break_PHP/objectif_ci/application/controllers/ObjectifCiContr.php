<?php

/**
* Objectif Ci controller class
*/
class ObjectifCiContr extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(TRUE);
		 $this->load->helper(array('form', 'url'));
		$this->load->model('ObjectifModel');
		$this->load->library('form_validation');
	}

	public function index(){
		$this->load->view('ObjectifCiView');
	}

	public function register(){
		$login = $this->input->post('entreprise');
		$pass = $this->input->post('pass');

		if ($this->input->post('submit')){
			$this->ObjectifModel->register($login,$pass);
			$this->load->view('ObjectifLogin');
		}
		else
		{
			echo 'Vous avez oublier de remplir des champs.';
		}
			
		/*$data = array(
			'login' => $this->input->post('login'),
			'password' => $this->input->post('password'),
			'entreprise'=> $this->input->post('entreprise');
		);*/
	}

	public function getLogin($login,$pass){		

		$login = $this->input->post('login');
		$pass = $this->input->post('pass');

		$this->ObjectifModel->login($login,$pass);

	}
		

	public function showView(){
		$this->load->view('ObjectifCiView');
	}
}