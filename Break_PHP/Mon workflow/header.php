<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="assets/ico/favicon.ico">

    <title>LookThis - RevoYle</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

    <!-- Ouverture nav -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="index.php">LookThis{</a>
        </div>
        <div class="navbar-collapse collapse navbar-right">
          <ul class="nav navbar-nav">
            <li class="active"><a href="index.php">Next</a></li>
            <li><a href="about.php">Mon savoir faire</a></li>
            <li><a href="contact.php">Détails</a></li>
              <ul class="dropdown-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">News<b class="caret"></b></a>
                <li><a href="blog.php">Mon blog</a></li>
                <li><a href="single-post.php">Evolution</a></li>
                <li><a href="portfolio.php">Galerie</a></li>
                <li><a href="single-project.php">Mes Projets</a></li>
                <li><?php include "/../home/nabilb/menu.php"; ?></li>
                <li><?php include "/../home/alexm/menu.php"; ?></li>
              </ul>
            </ul>
          <div>
            <button class="alert-dismissable">Message</button>
          </div>
        </div><!--/Fermuture nav -->
      </div>
    </div>