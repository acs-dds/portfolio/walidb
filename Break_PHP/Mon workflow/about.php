<?php require 'header.php'; ?>

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>A propos</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	 
	<!-- *****************************************************************************************************************
	 AGENCY ABOUT
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row">
	 		<div class="col-lg-6">
	 			<img class="img-responsive" src="assets/img/agency.jpg" alt="">
	 		</div>
	 		
	 		<div class="col-lg-6">
		 		<h4>Colaboration</h4>
		 		<p>Informations en cours </p>
		 		<p>Informations en cours.</p>
		 		<p>Informations en cours.</p>
 				<p><br/><a href="contact.php" class="btn btn-theme">Contacter-LookThis</a></p>
	 		</div>
	 	</div>
	 </div>

	<!-- *****************************************************************************************************************
	 TEEAM MEMBERS
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row centered">
		 	<h3 class="mb">Workers</h3>
		 	
		 	<div class="col-lg-3 col-md-3 col-sm-3">
				<div class="he-wrap tpl6">
				<img src="assets/img/team/team0.png" alt="">
					<div class="he-view">
						<div class="bg a0" data-animate="fadeIn">
                            <p class="ctitle" data-animate="fadeInDown">Laissez un message / share</p>
                            <a href="#" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-envelope"></i></a>
                            <a href="#" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-twitter"></i></a>
                    	</div><!-- he bg -->
					</div><!-- he view -->		
				</div><!-- he wrap -->
				<h4>walid Belbeche</h4>
				<h5 class="ctitle">LookThis{</h5>
				<p>Date de naissance : 28/12/1994 (22)</p>
				<p>Langues : Arabe , Anglais , Français</p>
				<p>Utilisation: Adobe , PackOffice , SublimeText , JavaScript, HTML 5 , CSS 3 , PHP</p>
				<p></p>
				<div class="hline"></div>
		 	</div>
		 	
		 	
	 	</div>
	 </div>
	 
<?php require 'history.php' ; ?>

	<!--
	 OUR CLIENTS
	 ***************************************************************************************************************** -->
	 <div id="cwrap">
		 <div class="container">
		 	<div class="row centered">
			 	<h3>OUR CLIENTS</h3>
			 	<div class="col-lg-3 col-md-3 col-sm-3">
			 		<img src="assets/img/clients/client01.png" class="img-responsive">
			 	</div>
			 	<div class="col-lg-3 col-md-3 col-sm-3">
			 		<img src="assets/img/clients/client02.png" class="img-responsive">
			 	</div>
			 	<div class="col-lg-3 col-md-3 col-sm-3">
			 		<img src="assets/img/clients/client03.png" class="img-responsive">
			 	</div>
			 	<div class="col-lg-3 col-md-3 col-sm-3">
			 		<img src="assets/img/clients/client04.png" class="img-responsive">
			 	</div>
		 	</div><! --/row -->
		 </div><! --/container -->
	 </div><! --/cwrap -->

<?php require 'footer.php'; ?>
	 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/retina-1.1.0.js"></script>
	<script src="assets/js/jquery.hoverdir.js"></script>
	<script src="assets/js/jquery.hoverex.min.js"></script>
	<script src="assets/js/jquery.prettyPhoto.js"></script>
  	<script src="assets/js/jquery.isotope.min.js"></script>
  	<script src="assets/js/custom.js"></script>


  </body>
</html>
