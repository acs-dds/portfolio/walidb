<?php require 'header.php'; require '/home/jeanp/xdb.php'; ?>

	<!-- *****************************************************************************************************************
	 Carousel
	 ***************************************************************************************************************** -->
		
	 <!-- 'carousel.php' --> 

	<!-- *****************************************************************************************************************
	 SERVICE LOGOS
	 ***************************************************************************************************************** -->
	<div id="recherche" class="formulaire">
		<h1>Production</h1>
		<div>
			<fieldset>
				<form method='post' action="traitement.php" class="panel panel-default >
					<label class="btn btn-primary" href="contact.php">Qui voulez-vous joindre ?</label>
					<select name="choix" class="choix">
						<option class="active">Selections</option>
						<option value="Nabil">Nabil</option>
						<option value="Alex">Alex</option>
						<option value="Walid">Walid</option>
					</select>
					<div class="champs">
						<input class="form-control" type="text" name="nom" placeholder="Nom">
						<input class="form-control" type="text" name="prénom" placeholder="Prénom">
							<span id="basic-addon1">@</span>
						<input class="form-control" type="text" name="mail" placeholder="e-mail">
						<input class="form-control" type="text" name="sujet" placeholder="sujet">
						<textarea class="form-control" type="text" rows="4" cols="30" name="message" placeholder="Votre demande"></textarea>
						<input class="btn btn-decondary btn-lg" type="submit" placeholder="Go"><br/>
					</div>
				</form>
			</fieldset>
		</div>
	</div>
<!-- 	<div class="alert alert-success" role="alert" class="détails"  name="err">
  		<strong>Erreur</strong> Merci de reinseignez les champs correctements .
	</div> -->
	<?php
		if(isset($_GET["reset"])){
			Xdb::reset();
		}
		$tableau = Xdb::get();



		foreach ( $tableau as $message ): ?>
			<table>
				<td> <?php echo 'Votre nom : ' .$message['nom']; ?> </td>
				<td> <?php echo 'Votre prénom :'.$message['prénom']; ?> </td>
				<td> <?php echo 'Votre e-mail :'.$message['mail']; ?> </td>
				<td> <?php echo 'Sujet :'.$message['sujet']; ?> </td>
				<td> <?php echo 'Votre demande :'.$message['message']; ?> </td>
			</table> 
		<?php endforeach; ?>

	<div id="service">
		<div class="container">
			<div class="row centered">
				<div class="col-md-4">
					<i class="fa fa-heart-o"></i>
					<h4>Votre Projet</h4>
					<p>Informations en cours.</p>
					<p><br/><a href="#" class="btn btn-theme">Plus de détails</a></p>
				</div>
				<div class="col-md-4">
					<i class="fa fa-flask"></i>
					<h4>Nouvelles résolutions</h4>
					<p>Informations en cours.</p>
					<p><br/><a href="#" class="btn btn-theme">Plus de détails</a></p>
				</div>
				<div class="col-md-4">
					<i class="fa fa-trophy"></i>
					<h4>Réalisation</h4>
					<p>Informations en cours.</p>
					<p><br/><a href="#" class="btn btn-theme">Plus de détails</a></p>
				</div>                      
			</div>
		</div><!--/container -->
	 </div> <!--/service -->
	 <img src="assets/img/ncv.jpg">
	 
	<!-- *****************************************************************************************************************
	 PORTFOLIO SECTION
	 ***************************************************************************************************************** -->
	<div id="portfoliowrap">
		<div class="portfolio-centered">
			<h3>Related Works.</h3>
			<div class="recentitems portfolio">
				<div class="portfolio-item graphic-design">
					<div class="he-wrap tpl6">
					<img src="assets/img/portfolio/portfolio_09.jpg" alt="">
						<div class="he-view">
							<div class="bg a0" data-animate="fadeIn">
								<h3 class="a1" data-animate="fadeInDown">A Graphic Design Item</h3>
								<a data-rel="prettyPhoto" href="assets/img/portfolio/portfolio_09.jpg" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-search"></i></a>
								<a href="single-project.html" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-link"></i></a>
							</div><!-- he bg -->
						</div><!-- he view -->      
					</div><!-- he wrap -->
				</div><!-- end col-12 -->
							
				<div class="portfolio-item web-design">
					<div class="he-wrap tpl6">
					<img src="assets/img/portfolio/portfolio_02.jpg" alt="">
						<div class="he-view">
							<div class="bg a0" data-animate="fadeIn">
								<h3 class="a1" data-animate="fadeInDown">A Web Design Item</h3>
								<a data-rel="prettyPhoto" href="assets/img/portfolio/portfolio_02.jpg" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-search"></i></a>
								<a href="single-project.html" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-link"></i></a>
							</div><!-- he bg -->
						</div><!-- he view -->      
					</div><!-- he wrap -->
				</div><!-- end col-12 -->
		
				<div class="portfolio-item graphic-design">
					<div class="he-wrap tpl6">
					<img src="assets/img/portfolio/portfolio_03.jpg" alt="">
						<div class="he-view">
							<div class="bg a0" data-animate="fadeIn">
								<h3 class="a1" data-animate="fadeInDown">A Graphic Design Item</h3>
								<a data-rel="prettyPhoto" href="assets/img/portfolio/portfolio_03.jpg" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-search"></i></a>
								<a href="single-project.html" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-link"></i></a>
							</div><!-- he bg -->
						</div><!-- he view -->      
					</div><!-- he wrap -->
				</div><!-- end col-12 -->
		
				<div class="portfolio-item graphic-design">
					<div class="he-wrap tpl6">
					<img src="assets/img/portfolio/portfolio_04.jpg" alt="">
						<div class="he-view">
							<div class="bg a0" data-animate="fadeIn">
								<h3 class="a1" data-animate="fadeInDown">A Graphic Design Item</h3>
								<a data-rel="prettyPhoto" href="assets/img/portfolio/portfolio_04.jpg" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-search"></i></a>
								<a href="single-project.html" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-link"></i></a>
							</div><!-- he bg -->
						</div><!-- he view -->      
					</div><!-- he wrap -->
				</div><!-- end col-12 -->

				<div class="portfolio-item graphic-design">
					<div class="he-wrap tpl6">
					<img src="assets/img/portfolio/portfolio_05.jpg" alt="">
						<div class="he-view">
							<div class="bg a0" data-animate="fadeIn">
								<h3 class="a1" data-animate="fadeInDown">A Graphic Design Item</h3>
								<a data-rel="prettyPhoto" href="assets/img/portfolio/portfolio_04.jpg" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-search"></i></a>
								<a href="single-project.html" class="dmbutton a2" data-animate="fadeInUp"><i class="fa fa-link"></i></a>
							</div><!-- he bg -->
						</div><!-- he view -->      
					</div><!-- he wrap -->
				</div><!-- end col-12 -->
										
					
			</div><!-- portfolio -->
		</div><!-- portfolio container -->
	</div><!--/Portfoliowrap -->
	 
	 
	<!-- *****************************************************************************************************************
	 MIDDLE CONTENT
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
		<div class="row">
			<div class="col-lg-4 col-lg-offset-1">
				<h4>BONUS : </h4>
				<p>En cours </p>
				<p><br/><a href="about.php" class="btn btn-theme">Détails</a></p>
			</div>
			
			<div class="col-lg-3">
				<h4>Question/Reponses</h4>
				<div class="hline"></div>
				<p><a href="#">Question1</a></p>
				<p><a href="#">Question2</a></p>
				<p><a href="#">Question3</a></p>
				<p><a href="#">Question4</a></p>
				<p><a href="#">Question5</a></p>
			</div>
			
			<div class="col-lg-3">
				<h4>Articles</h4>
				<div class="hline"></div>
				<p><a href="single-post.php">Nouveauté</a></p>
				<p><a href="single-post.php">Nouveauté</a></p>
				<p><a href="single-post.php">Nouveauté</a></p>
				<p><a href="single-post.php">Nouveauté</a></p>
				<p><a href="single-post.php">Nouveauté</a></p>
			</div>
			
		</div><!--/row -->
	 </div><!--/container -->
	 
<?php require 'history.php'; ?>
	 
	<!-- *****************************************************************************************************************
	 OUR CLIENTS
	 ***************************************************************************************************************** -->
	 <div id="cwrap">
		 <div class="container">
			<div class="row centered">
				<h3>Collections</h3>                 
				   </div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<a href="eprojets/simon/index.html"> <img class="simon" src="assets/img/simon.jpg"></a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<a href="eprojets/Fourchette.html"><img class="four" src="assets/img/fourchette.jpeg"></a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<a href="eprojets/margin/margin.html"><img class="margin" src="assets/img/margin.png"></a>
				</div>
				<div class="col-lg-3 col-md-3 col-sm-3">
					<a href="eprojets/Mini-calculatrice.html"> <img class="simon" src="assets/img/calc.png"></a>
				</div>
			</div><!--/row -->
		 </div><!--/container -->
	 </div><!--/cwrap -->

<?php require 'footer.php' ;?>
	 
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/ajax_jq.js"></script>
	<!-- <script src="assets/js/bootstrap.min.js"></script> -->
	<script src="assets/js/jquery.js"></script>
	<script src="assets/js/retina-1.1.0.js"></script>
	<script src="assets/js/jquery.hoverdir.js"></script>
	<script src="assets/js/jquery.hoverex.min.js"></script>
	<script src="assets/js/jquery.prettyPhoto.js"></script>
	<script src="assets/js/jquery.isotope.min.js"></script>
	<script src="assets/js/custom.js"></script>


	<script>

	// Transition page
	$(document).ready(function(){
		$("body").css("display", "none");
		$("body").fadeIn(3003);
		$("a.transition").click(function(event) {
				event.preventDefault();
				linkLocation = this.href;
				$("body").fadeOut(1000, redirectPage);
		})
		function redirectPage() {
			windows.location = linkLocation;
		}
	});
	
		// Portfolio
		(function($) {
		"use strict";
		var $container = $('.portfolio'),
		$items = $container.find('.portfolio-item'),
		portfolioLayout = 'fitRows';
		
		if( $container.hasClass('portfolio-centered') ) {
			portfolioLayout = 'masonry';
		}
				
		$container.isotope({
			filter: '*',
			animationEngine: 'best-available',
			layoutMode: portfolioLayout,
			animationOptions: {
			duration: 750,
			easing: 'linear',
			queue: false
		},
		masonry: {
		}
		}, refreshWaypoints());
		
		function refreshWaypoints() {
			setTimeout(function() {
			}, 1000);   
		}
				
		$('nav.portfolio-filter ul a').on('click', function() {
				var selector = $(this).attr('data-filter');
				$container.isotope({ filter: selector }, refreshWaypoints());
				$('nav.portfolio-filter ul a').removeClass('active');
				$(this).addClass('active');
				return false;
		});
		
		function getColumnNumber() { 
			var winWidth = $(window).width(), 
			columnNumber = 1;
		
			if (winWidth > 1200) {
				columnNumber = 5;
			} else if (winWidth > 950) {
				columnNumber = 4;
			} else if (winWidth > 600) {
				columnNumber = 3;
			} else if (winWidth > 400) {
				columnNumber = 2;
			} else if (winWidth > 250) {
				columnNumber = 1;
			}
				return columnNumber;
			}       
			
			function setColumns() {
				var winWidth = $(window).width(), 
				columnNumber = getColumnNumber(), 
				itemWidth = Math.floor(winWidth / columnNumber);
				
				$container.find('.portfolio-item').each(function() { 
					$(this).css( { 
					width : itemWidth + 'px' 
				});
			});
		}
		
		function setPortfolio() { 
			setColumns();
			$container.isotope('reLayout');
		}
			
		$container.imagesLoaded(function () { 
			setPortfolio();
		});
		
		$(window).on('resize', function () { 
		setPortfolio();          
		});
		})(jQuery);
	</script>
  </body>
</html>
