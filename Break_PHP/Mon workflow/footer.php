	<!-- *****************************************************************************************************************
	 FOOTER
	 ***************************************************************************************************************** -->
	 <div id="footerwrap">
	 	<div class="container">
		 	<div class="row">
		 		<div class="col-lg-4">
		 			<h4>Qui suis-je</h4>
		 			<div class="hline-w"></div>
		 			<p>Informations en cours.</p>
		 		</div>
		 		<div class="col-lg-4">
		 			<h4>Reseau sociaux</h4>
		 			<div class="hline-w"></div>
		 			<p>
		 				<a href="#"><i class="fa fa-facebook"></i></a>
		 				<a href="#"><i class="fa fa-twitter"></i></a>
		 				<a href="#"><i class="fa fa-instagram"></i></a>
		 			</p>
		 		</div>
		 		<div class="col-lg-4">
		 			<h4>QG{</h4>
		 			<div class="hline-w"></div>
		 			<p>
		 				Home,<br/>
		 				Work,<br/>
		 				France.<br/>
		 			</p>
		 		</div>
		 	
		 	</div><!--/row -->
	 	</div><!--/container -->
	 </div><!--/footerwrap -->