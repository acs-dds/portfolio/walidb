	<!-- *****************************************************************************************************************
	 TITLE & CONTENT
	 ***************************************************************************************************************** -->
	 <br/>
	 <div class="container mt">
	 	<div class="row">
		 	<div class="col-lg-10 col-lg-offset-1 centered">
			 	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				  <!-- Indicators -->
				  <ol class="carousel-indicators">
				    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
				    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
				  </ol>
				
				  <!-- Wrapper for slides -->
				  <div class="carousel-inner">
				    <div class="item active">
				      <img src="assets/img/portfolio/single01.jpg" alt="">
				    </div>
				    <div class="item">
				      <img src="assets/img/portfolio/single02.jpg" alt="">
				    </div>
				    <div class="item">
				      <img src="assets/img/portfolio/single03.jpg" alt="">
				    </div>
				  </div>
				</div><!--/Carousel -->
		 	</div>