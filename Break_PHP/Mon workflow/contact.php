<?php require 'header.php'; ?>

	<!-- *****************************************************************************************************************
	 BLUE WRAP
	 ***************************************************************************************************************** -->
	<div id="blue">
	    <div class="container">
			<div class="row">
				<h3>Prendre contact</h3>
			</div><!-- /row -->
	    </div> <!-- /container -->
	</div><!-- /blue -->

	<!-- *****************************************************************************************************************
	 CONTACT WRAP
	 ***************************************************************************************************************** -->

	 <div id="contactwrap"></div>
	 
	<!-- *****************************************************************************************************************
	 CONTACT FORMS
	 ***************************************************************************************************************** -->

	 <div class="container mtb">
	 	<div class="row">
	 		<div class="col-lg-8">
	 			<h4>Je vous invite à remplir ce formulaire</h4>
	 			<div class="hline"></div>
		 			<p>Une fois que vous avez terminer , n'oublie pas de valider votre reponse afin d'être rediriger.</p>
		 			<p>MessagePopup a crée</p>
		 			<form role="form">
					  <div class="form-group">
					    <label for="InputName1">Nom</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					    <label for="InputEmail1">Prénom</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					    <label for="InputSubject1">Sujet</label>
					    <input type="email" class="form-control" id="exampleInputEmail1">
					  </div>
					  <div class="form-group">
					  	<label for="message1">Message</label>
					  	<textarea class="form-control" id="message1" rows="3"></textarea>
					  </div>
					  <button type="submit" class="btn btn-theme">Transmetre</button>
					</form>
			</div>
	 		
	 		<div class="col-lg-4">
		 		<h4>Nos locaux</h4>
		 		<div class="hline"></div>
		 			<p>
		 				En cours,<br/>
		 			</p>
		 			<p>
		 				Email: belbeche.w@gmail.com<br/>
		 				Tel: 06.66.00.33.31
		 			</p>
		 			<p>Auto-Entrepreneur</p>
	 		</div>
	 	</div>
	 </div>


<?php require 'footer.php'; ?>
	 
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/retina-1.1.0.js"></script>
	<script src="assets/js/jquery.hoverdir.js"></script>
	<script src="assets/js/jquery.hoverex.min.js"></script>
	<script src="assets/js/jquery.prettyPhoto.js"></script>
  	<script src="assets/js/jquery.isotope.min.js"></script>
  	<script src="assets/js/custom.js"></script>


  </body>
</html>
