<?php 

/**
* Usermodel Class
*/
class UserModel extends CI_Model
{
	
	function __construct()
	{
		parent::__construct();
	}

	public function getUser($login,$mdp){
		$req = "SELECT id, entreprise FROM utilisateur WHERE entreprise = ? AND mdp = ?;";
		return $this->db->query($req, [$login, hash('sha512', "ok@/$mdp")])->row();
	}
}