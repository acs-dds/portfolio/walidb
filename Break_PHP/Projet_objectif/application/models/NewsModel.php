<?php 

/**
* Class Model ObjectifCi
*/
class NewsModel extends CI_Model
{
	private $identreprise;
	
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function setIdEntreprise($identreprise){
		$this->identreprise = $identreprise;
	}
	private function ok(){
		return isset($this->identreprise);
	}

	public function AllSucces($succes){
		$succes = array();
		if($this->ok())
			return $this->db->query("SELECT id, titre,intitule, objectif, progression FROM succes WHERE idutilisateur = ? ORDER BY id ASC",[$this->identreprise])->result();
	}

	public function getSucces($id){
		if ($this->ok())
			return $this->db->query("SELECT id, titre , intitule, objectif , progression FROM succes WHERE idutilisateur = ? AND id = ?",[$this->identreprise, $id])->row();
	}

	public function updateSucess($id,$progression){
		if ($this->ok())
			$this->db->query("UPDATE sucess SET progression = ? WHERE id = ? AND idutilisateur = ?", [$progression, $id , $this->identreprise]);
	}

	/*public function login($login,$pass){

		$data = array(
			'entreprise' => $login,

		);

			//$this->input->post('submit')
			$this->db->select('entreprise','mdp');
			$this->db->from('utilisateur');
			return $this->db->get();
		}
	}*/
}