<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
* News Controllers
*/
class News extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->helper('url_helper');
	}

	public function connexion(){
		$data = [];
		if(!is_null($this->input->post('login'))){
			// envoi du form
			$this->load->model('UserModel');

			//
			$user = $this->UserModel->getUser($this->input->post('login'),$this->input->post('mdp'));
			if(!empty($user)){
				$this->session->utilisateur = $user;
			$data['message'] = "Pas de compte sur la BDD";
			}

			$this->load->view('templates/header');
			$this->load->view('forms/signin',$data);
			$this->load->view('templates/footer');
			// Affichage du form
		}
	}

	public function deconnexion(){
		$this->session->unset_userdata('utilisateur');
		redirect('connexion');
	}

	public function index(){
		$this->access->restrict();
		$this->load->model('NewsModel');
		$this->NewsModel->setIdEntreprise->($this->session->utilisateur->id);

		$this->load->view('templates/header', ['utilisateur' => $this->session->utilisateur->entreprise]);

		$this->load->view('index', ['succes' => $this->NewsModel->AllSucces()]);

		$this->load->view('templates/footer');
	}

	public function view ($id){
		$this->access->restrict();
		$this->load->model('NewsModel');
		$this->NewsModel->setIdEntreprise->($this->session->utilisateur->id);

		$this->NewsModel->setIdEntreprise($this->session->utilisateur->id);
		$this->load->view('templates/header', ['utilisateur' => $this->session->utilisateur->entreprise]);
		$this->load->view('viewSucces', ['succes' => $this->NewsModel->getSucces($id)]);

		$this->load->view('templates/footer');
	}
	public function update($id){
		$this->access->restrict();
		$this->load->model('NewsModel');
		$this->NewsModel->setIdEntreprise($this->session->utilisateur->id);

		$this->NewsModel->updateSucess($id, $this->input->post('progression'));
		redirect('News/view'.$id);
	}
	public function getSuc($succes){
		return $this->NewsModel->AllSucces($succes);
	}
}