
<?php 

/**
* Class controller
*/
class Pages extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->helper('url');
	}

	public function view($page = 'home'){

 		if(!File_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
            // Oups, nous n'avons pas de page pour cela!
			Show_404();
        }
        else
        {
         // Mettre en majuscule la première lettre
         $data['title'] = ucfirst($page);

         $this->load->view('templates/header',$data);
         $this->load->view('pages/'.$page , $data);
         $this->load->view('templates/footer',$data);
        }
	}
}