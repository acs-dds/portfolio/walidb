<div class="col-xs-12 achievo-list">
	<?php foreach ($succes as $s): ?>
	<article>
		<h2><?php echo $s->titre; ?></h2>
		<p><?php echo $s->intitule; ?></p>
		<?php if ($s->progression == $s->objectif): ?>
		<p>We did it !</p>
		<?php endif; ?>
		<a href="<?php echo site_url('succes/'.url_title($s->titre, '-', true).'-'.$s->id); ?>">Voir l'objectif</a> 
	</article>
	<?php endforeach; ?>
</div>