<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PrincipalController extends CI_Controller {

	/**
	 * PrincipalController 
	 */
	public function __construct()
	{
		parent::__construct();
		$this->output->enable_profiler(TRUE);
		$this->load->model('PrincipalModel');
	}

	public function getListeSucces()
	{
			// Je récupere mes colones sous forme de liste
			// et je les passe en parametre dans la vue.
		$data['liste'] = $this->PrincipalModel->listeSucces();

		$this->load->view('Principal', $data);
	}
	public function view($id){
		$id3['id'] = $this->PrincipalModel->getId($id);
		$this->session->set_userdata("iduser", $id);
		//$input = $this->input->post('update');
		$this->load->view('Create', $id3);
	}

	public function update(){
		$update = $this->input->post('update');

		$id2['id2'] = $this->PrincipalModel->listeSucces();

		if (isset($update)) {
			return $this->PrincipalModel->reqUpdate($this->session->iduser,$update);
		}
		$this->load->view('Create');
	}
	public function login(){
		$this->load->view('login',$_SESSION['user']);
		$login = $this->session->login;
		$_SESSION['login'] = $this->session->set_userdata('login',$login);
		var_dump($login);
		$_SESSION['status'] = "Connexion encours";
		//$this->input->post('login');
		//$this->input->post('mdpp');
	}
}