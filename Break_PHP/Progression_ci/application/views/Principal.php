<!DOCTYPE html>
<html>
<head>
	<title>Objectifs</title>
	<link rel="stylesheet" type="text/css" href="\Projet_objectif\assets\css\bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="\Projet_objectif\assets\css\styles.css">
	<script type="text/javascript" src="\Projet_objectif\assets\js\jquery-3.1.1.min.js"></script>
	<script type="text/javascript" src="\Projet_objectif\assets\js\bootstrap.js"></script>
</head>
<body>


<center><h1>Objectifs</h1>
	<div class="container">
		<div class="well well-lg">
				<?php
				foreach($liste as $row):{?>
					<article class="list-group">
					<h2> <?= $row['titre'].'</br>';?></h2>
					<p><?= $row['intitule'].'</br>';?></p>
					<progress value="<?= $row['progression'];?>" max="<?= $row['objectif'];?>"></progress>
					<aside><?= $row['idutilisateur'];?></aside>
					</article>
					<a href="index.php/PrincipalController/view/<?= $row['idutilisateur']?>"><button>View</button></a> 
				<?php
				}
			?>
			<?php endforeach;?>
		</div>
	</div>
</center>
</body>
</html>