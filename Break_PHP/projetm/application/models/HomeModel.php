<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Model class
* 
* Funtion : fetchLogin (req);
* Function : register user (req);
* Function : stocks articles (req);
* Function : livre d'or (req);
* Instruction : 
* Function login : request query -> en objet stoquer par ligne dans un array contenant 2variables
* Function : 
*/
class HomeModel extends CI_Model
{
	function __construct()
	{
		parent::__construct();
		$this->load->database();
	}

	public function login($login,$pass){
			//J'effectue ma request pour chercher si des login correspond à la saisi 

		$query = $this->db->query("SELECT * FROM login WHERE name = ? AND mdp = ?",array($login,$pass));

		$row = $query->row();
			// Je vérifie si j'ai récuperer des informations sur query.
			// (Si oui alors on affiche dans le contenu de la variable suivi de la colone souhaiter)
		if (isset($row)){
			return "Utilisateur: ".$row->name."<br/>";
			//echo "Mot de passe: ".$row->mdp."<br/>";
			//echo "Status: ".$row->status."<br/>";
		}
		else 
		{
			return "<center>"."<b>"."<b>Identifients introuvable, Veuillez vous enregistrer"."</b>"."</center>";
		}
	}
}
