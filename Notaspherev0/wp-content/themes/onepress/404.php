<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package OnePress
 */

get_header(); ?>
	<div id="content" class="site-content">

		<div class="page-header">
			<div class="container">
				<h1 class="page-title"><?php esc_html_e( 'Informations indisponible : erreur 404', 'onepress' ); ?></h1>
			</div>
		</div>

		<div id="content-inside" class="container right-sidebar">
			<div id="primary" class="content-area">
				<main id="main" class="site-main" role="main">

					<section class="error-404 not-found">

						<div class="page-content">
							<div id="erreur">
								<p><?php esc_html_e( '... ,Mauvaise manipulation ou bien la page a été retiré', 'onepress' ); ?>	</p>
							</div>

							<?php if ( onepress_categorized_blog() ) : // On affiche les widgets quand le site possède plusieurs catégories. ?>

							<?php endif; ?>

							<?php
								/* traduction: %1$s: smiley */
								$archive_content = '<p>' . sprintf( esc_html__( 'Try looking in the monthly archives. %1$s', 'onepress' ), convert_smilies( ':)' ) ) . '</p>';
								the_widget( 'WP_Widget_Archives', 'dropdown=1', "after_title=</h2>$archive_content" );
							?>

							<?php the_widget( 'WP_Widget_Tag_Cloud' ); ?>

						</div><!-- .page-content -->
					</section><!-- .error-404 -->

				</main><!-- #main -->
			</div><!-- #primary -->

			

		</div><!--#content-inside -->
	</div><!-- #content -->
	
<?php get_footer(); ?>
