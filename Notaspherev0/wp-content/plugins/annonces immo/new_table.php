<?php 
/*
* Modification la base SQL statement 
*
* utile pour crée , et mettre à jour une structure de table 
* wpdb  $wpdb()
*/

/*Création d'une table avec une fonction*/

global $dev_on_versions;
$dev_on_versions = '1.0';

function dev_on() {
	global $wpdb;
	global $dev_on_versions;

	$table_name = $wpdb->prefix."newlife";
 	$charset_collate = $wpdb->get_charset_collate();

	// dans la variable SQL on crée la table depuis $table name  
	 $sql ​​= "CREATE TABLE $table_name (
	   id mediumint(9) NOT NULL AUTO_INCREMENT,
	   time datetime DEFAULT '0000-00-00 00:00:00' NOT NULL,
	   name tinytext NOT NULL,
	   text text NOT NULL,
	   url varchar(55) DEFAULT '' NOT NULL,
	   PRIMARY KEY (id)
	) $charset_collate; ";

	require_once(ABSPATH. 'wp-admin/includes/upgrade.php');
	dbDelta( $sql );

	add_option('dev_on_versions', $dev_on_versions);
	/*Appele de la version installer actuellement */
	$installed_ver = get_option("dev_on_versions"); 

	if ($installed_ver != $dev_on_versions) {
		$table_name = $wpdb->prefix.'newlife';
		$sql = "CREATE TABLE $table_name (
			id mediumint(9) NOT NULL AUTO_INCREMENT,
			time datetime default '0000-00-00 00:00:00' NOT NULL,
			name tinytext NOT NULL,
			text text NOT NULL,
			url varchar(100) DEFAULT '' NOT NULL,
			PRIMARY KEY(id)
		);";

	require_once (ABSPATH. 'wp-admin/includes/upgrade.php');
	dbDelta($sql);

	update_option("dev_on_versions",$dev_on_versions);
}

function myplugin_update_db_check() {
	global $dev_on_versions;
	
	if (get_site_option('dev_on_versions') != $dev_on_versions) {
		dev_on();
	}
}
add_action('plugins_loaded','myplugin_update_db_check');
}

/* Ajout de données au tableau */

function dev_on_data() {
	global $wpdb;

	$welcome_name = 'Mr. WordPress';
	$welcome_text = 'Félicitations, votre premier plugin ';

	$table_name = $wpdb->prefix.'newlife'; 

	$wpdb->insert(
	$table_name,
		array(
			'time' => current_time('mysql'),
			'name' => $welcome_name,
			'text' => $welcome_text,
		)
	);
}

/*Function de mise à niveau, si la versions du plugins as été modifier*/
