<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'dubosson');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'dubosson');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'dubosson123');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', 'localhost:5432');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+1jwPzSu#H=>3`oi~c(E3Sg*c.kD3_XoZg~95nU:q+J6J*UsXcuY=J 0/#uY; 7C');
define('SECURE_AUTH_KEY',  'DF^0Qxn@Sw`hMC@LOc]K|C&b0@_yB1tIS_DA>19buA9Hm}+v/vh%c#{I2I96 -DZ');
define('LOGGED_IN_KEY',    '_u8zVccp/f&iYTxEM8g0rFflE9#rm8o$>7oV]y&)(<eG]52.C$7hoVCW8sc^t`/5');
define('NONCE_KEY',        '_K.YYOA*,|1`tp&BG;,=>>(Upva1*oxp._%~_CDNvi7fN.J_}(2z:)@ORH bM(S(');
define('AUTH_SALT',        'o4Z-f:H6fhS`=8@.hQ0P0v4.24>IPp//^Yc/|&$$p69nF4Imm98l5BnRqWuK}VUS');
define('SECURE_AUTH_SALT', '>D{CyWBxT6;V r)NPw|U4nd[u?q#LGEm.V@9tteg9%LBBGT$[(, .qTV@~3+o2_E');
define('LOGGED_IN_SALT',   '?uX7LC=yZuF_&ljtP2WxHA1e{$5s^{gmDIHtG5?rXajZtL4~*>b1d2sEI~v{xKkA');
define('NONCE_SALT',       'jO0?0i_A<8 ij#mK|#A0Ku:xT#(pp2C{o=`u19Dn0JL%:vq]wP]u8w*lu?:;`F51');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = '148148_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');
define('WP_CACHE', true);