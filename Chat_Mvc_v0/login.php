<?php require 'includes/header.php'; ?>
<?php include_once 'includes/nav_bar.php';?>
  <body>

		  <article id="login_form">
        <div class="container"> <!-- Login_Form -->
          <form action="includes/trait_login.php" method="post">
            <div class="form-group row">
              <label for="inputEmail3" class="col-sm-2 col-form-label">Email</label>
            <div class="col-sm-10">
              <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
            </div>
            </div>
            <div class="form-group row">
              <label for="inputPassword3" class="col-sm-2 col-form-label">Password</label>
            </div>
            <div class="col-sm-10">
              <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
          </form>
        </div>
       <!-- Fin_Login_Form -->
      </article>
      <!-- Début choix radio -->
    <fieldset class="form-group row">
      <a href="includes/regles.php"></a><legend class="col-form-legend col-sm-2">Reglement</legend>
      <div class="col-sm-10">
        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios1" value="option1" checked>
            Je suis majeure (18+).
          </label>
        </div>
        <div class="form-check">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios2" value="option2">
            Je suis mineur(18-).
          </label>
        </div>
        <div class="form-check disabled">
          <label class="form-check-label">
            <input class="form-check-input" type="radio" name="gridRadios" id="gridRadios3" value="option3" disabled>
            Autres choix(indisponible).
          </label>
        </div>
      </div>
    </fieldset> <!-- Fin choix radio -->
      <div class="form-group has-success">
        <label class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input">
          <span class="custom-control-indicator"></span>
          <span class="custom-control-description">Check this custom checkbox</span>
        </label>
      </div>
    <div class="form-group row">
      <div class="offset-sm-2 col-sm-10">
        <button type="submit" class="btn btn-primary">Connexion</button>
      </div>
    </div>
</div>
    <div class="inner cover">
      <h1 class="cover-heading">Rejoindre la communauté ||Chat_World||</h1>
      <p class="lead">Cliquez sur le boutton en dessous pour nous rejoindre =).</p>
      <p class="lead">
        <a href="#" class="btn btn-lg btn-secondary">INSCRIPTION</a>
      </p>
    </div>
	<!-- JavaScript -->
<!--   <script src="js/tether.min.js"</script>  -->

	


<?php require 'includes/footer.php'; ?>