<?php include_once '../includes/header.php';?>
<?php require '../includes/nav_bar.php';
?>
<body>
		<div class="wrapper"> 
			<article>
				<form id="form">
					<input type="text" name="user" placeholder="Votre pseudo">
					<input id="msg" type="text" name="message" placeholder="Message">
					<input type="submit" id="tchater" value="Tchatter">
				</form>
				<div id="tchat"></div>
			</article>
		</div> <!-- Fin wrapper -->
			<aside> <!-- Membres en ligne -->
				<div id="membre"><h2>Membres</h2>
					<button id="online">Voir qui est en ligne</button>
				</div>
			</aside> <!-- Fin Membres -->

			<!-- JavaScript -->
			<script type="text/javascript" src="../js/refresh.js"></script>
			<script>
			$(document).ready(function(){
				$("button").click(function(){
					$("#membre").load("test.php");
						$('#aside').fadeToggle('500');
					});
				});
			</script>

<?php require '../includes/footer.php';?>