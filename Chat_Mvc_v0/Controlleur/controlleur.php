<?php
require_once '../Model/mapper.php';

/**
* Controlleur => Chat_World
*	Function: 
*	Function:
*	Function:
*/

class ChatContr
{
	public $model;
	public $user_session;

	public function __construct(){
		$this->model = new ChatModel();
	}
	 
	public function AddMessage($pseudo,$message){

		return $this->model->message($pseudo,$message);
	}

	public function getMsg(){

		return $this->model->getMsg();
	}

/*	public function register($user_session){
		$_SESSION['nom'] = $user_session;
	}*/


	public function readMsg(){
		return $this->model->read();
	}
	/*public function delet_line($id){
		return $this->model->delet($id);
	}*/
	public function deletMsg($id){
		return $this->model->delet($id);
	}
}
