                        <?php $post_classes = ( ! empty($post_classes) )? $post_classes :  ''; ?>
                        <?php $no_media = ( ! ( $has_attach ) )? ' no-media' :  '' ?>

                        <article <?php post_class( $post_classes . $no_media ); ?> >
                            <?php if ( $has_attach ) : ?>

                            <div class="entry-media">
                                <?php echo $the_media; ?>
                            </div>
                            <?php endif; ?>

                            <div class="entry-main">

                                <header class="entry-header">
                                <?php
                                if( $show_title ) {
                                ?>
                                    <h4 class="entry-title">
                                        <a href="<?php echo !empty( $the_link ) ? $the_link : get_permalink(); ?>">
                                            <?php echo !empty( $the_name ) ? $the_name : get_the_title(); ?>
                                        </a>
                                    </h4>
                                <?php
                                } ?>
                                </header>

                                <?php if( $show_info ) { ?>
                                <div class="entry-info">
                                    <div class="entry-date">
                                        <a href="<?php the_permalink(); ?>">
                                            <time datetime="<?php the_time('Y-m-d')?>" title="<?php the_time('Y-m-d')?>" ><?php the_time('F j, Y'); ?></time>
                                        </a>
                                    </div>
                                    <span class="entry-tax">
                                        <?php // the_category(' '); we can use this template tag, but customizable way is needed! ?>
                                        <?php $tax_name = 'category';
                                              if( $cat_terms = wp_get_post_terms( $post->ID, $tax_name ) ){
                                                  foreach( $cat_terms as $term ){
                                                      echo '<a href="'. get_term_link( $term->slug, $tax_name ) .'" title="'.__("View all posts in ", 'phlox'). $term->name .'" rel="category" >'. $term->name .'</a>';
                                                  }
                                              }
                                        ?>
                                    </span>
                                    <?php edit_post_link(__("Edit", 'phlox'), '<i> | </i>', ''); ?>
                                </div>
                                <?php } ?>

                            </div>

                        </article>
