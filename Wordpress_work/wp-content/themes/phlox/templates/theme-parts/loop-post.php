<?php /* Loops through all posts, taxes, .. and display posts */

global $query_string;

// print the post slider
echo auxin_get_the_archive_slider( 'post', 'content' );

// the page number
$paged            = max( 1, get_query_var('paged'), get_query_var('page') );
// get template type id
$template_type_id = auxin_get_option( 'post_index_template_type', 'default' );
// posts perpage
$per_page         = get_option( 'posts_per_page' );

// if template type is masonry
if( 6 == $template_type_id ){

    $args = array(
        'exclude_without_media'         => auxin_get_option( 'exclude_without_media' ),
        'exclude_custom_post_formats'   => 0,
        'exclude_quote_link'            => auxin_get_option( 'post_exclude_quote_link_formats', 1 ),
        'display_like'                  => auxin_get_option( 'show_blog_archive_like_button', 1 ),
        'show_media'                    => true,
        'show_excerpt'                  => true,
        'excerpt_len'                   => auxin_get_option( 'blog_content_on_listing_length' ),
        'show_info'                     => true,
        'author_or_readmore'            => 'readmore',
        'desktop_cnum'                  => auxin_get_option( 'post_index_column_number' ),
        'tablet_cnum'                   => auxin_get_option( 'post_index_column_number_tablet' ),
        'phone_cnum'                    => auxin_get_option( 'post_index_column_number_mobile' ),
        'tag'                           => '',
        'extra_classes'                 => '',
        'custom_el_id'                  => '',
        'reset_query'                   => false,
        'use_wp_query'                  => true,
    );

    if( function_exists( 'auxin_widget_recent_posts_masonry_callback' ) ){
        // get the shortcode base blog page
        $result = auxin_widget_recent_posts_masonry_callback( $args );
    } else {
        $result = sprintf( __('To enable this feature, please install %s plugin.', 'phlox' ), '<a href="'.admin_url('plugin-install.php?s=auxin&tab=search&type=term').'" target="_blank">' . __('Auxin Elements', 'phlox' ) . '</a>' );
    }


// if template type is tiles
} elseif( 9 == $template_type_id ){

    $args = array(
        'exclude_without_media'         => auxin_get_option( 'exclude_without_media' ),
        'exclude_custom_post_formats'   => 0,
        'exclude_quote_link'            => auxin_get_option( 'post_exclude_quote_link_formats', 1 ),
        'show_media'                    => true,
        'show_excerpt'                  => true,
        'excerpt_len'                   => auxin_get_option( 'blog_content_on_listing_length' ),
        'display_title'                 => true,
        'show_info'                     => true,
        'author_or_readmore'            => 'readmore',
        'tag'                           => '',
        'extra_classes'                 => '',
        'custom_el_id'                  => '',
        'reset_query'                   => false,
        'use_wp_query'                  => true
    );


    if( function_exists( 'auxin_widget_recent_posts_tiles_callback' ) ){
        // get the shortcode base blog page
        $result = auxin_widget_recent_posts_tiles_callback( $args );
    } else {
        $result = sprintf( __('To enable this feature, please install %s plugin.', 'phlox' ), '<a href="'.admin_url('plugin-install.php?s=auxin&tab=search&type=term').'" target="_blank">' . __('Auxin Elements', 'phlox' ) . '</a>' );
    }

// if template type is land
} elseif( 8 == $template_type_id ){

    $args = array(
        'exclude_without_media'         => auxin_get_option( 'exclude_without_media' ),
        'exclude_custom_post_formats'   => 0,
        'exclude_quote_link'            => auxin_get_option( 'post_exclude_quote_link_formats', 1 ),
        'show_media'                    => true,
        'display_like'                  => auxin_get_option( 'show_blog_archive_like_button', 1 ),
        'show_excerpt'                  => true,
        'excerpt_len'                   => auxin_get_option( 'blog_content_on_listing_length' ),
        'display_title'                 => true,
        'show_info'                     => true,
        'author_or_readmore'            => 'readmore',
        'image_aspect_ratio'            =>  auxin_get_option( 'post_image_aspect_ratio' ),
        'tag'                           => '',
        'extra_classes'                 => '',
        'custom_el_id'                  => '',
        'reset_query'                   => false,
        'use_wp_query'                  => true
    );


    if( function_exists( 'auxin_widget_recent_posts_land_style_callback' ) ){
        // get the shortcode base blog page
        $result = auxin_widget_recent_posts_land_style_callback( $args );
    } else {
        $result = sprintf( __('To enable this feature, please install %s plugin.', 'phlox' ), '<a href="'.admin_url('plugin-install.php?s=auxin&tab=search&type=term').'" target="_blank">' . __('Auxin Elements', 'phlox' ) . '</a>' );
    }

// if template type is timeline
} elseif( 7 == $template_type_id ){

    $args = array(
        'exclude_without_media'         => auxin_get_option( 'exclude_without_media' ),
        'exclude_custom_post_formats'   => 0,
        'exclude_quote_link'            => auxin_get_option( 'post_exclude_quote_link_formats', 1 ),
        'show_media'                    => true,
        'display_like'                  => auxin_get_option( 'show_blog_archive_like_button', 1 ),
        'show_excerpt'                  => true,
        'excerpt_len'                   => auxin_get_option( 'blog_content_on_listing_length' ),
        'display_title'                 => true,
        'show_info'                     => true,
        'author_or_readmore'            => 'readmore',
        'image_aspect_ratio'            => auxin_get_option( 'post_image_aspect_ratio' ),
        'timeline_alignment'            => auxin_get_option( 'post_index_timeline_alignment', 'center' ),
        'tag'                           => '',
        'reset_query'                   => false,
        'use_wp_query'                  => true
    );

    if( function_exists( 'auxin_widget_recent_posts_timeline_callback' ) ){
        // get the shortcode base blog page
        $result = auxin_widget_recent_posts_timeline_callback( $args );
    } else {
        $result = sprintf( __('To enable this feature, please install %s plugin.', 'phlox' ), '<a href="'.admin_url('plugin-install.php?s=auxin&tab=search&type=term').'" target="_blank">' . __('Auxin Elements', 'phlox' ) . '</a>' );
    }

// if template type is grid
} elseif( 5 == $template_type_id ){

    $args = array(
        'exclude_without_media'         => auxin_get_option( 'exclude_without_media' ),
        'exclude_custom_post_formats'   => 0,
        'exclude_quote_link'            => auxin_get_option( 'post_exclude_quote_link_formats', 1 ),
        'show_media'                    => true,
        'show_excerpt'                  => true,
        'display_like'                  => auxin_get_option( 'show_blog_archive_like_button', 1 ) ,
        'content_layout'                => auxin_get_option( 'post_index_column_content_layout', 'full' ) ,
        'excerpt_len'                   => auxin_get_option( 'blog_content_on_listing_length' ),
        'display_title'                 => true,
        'show_info'                     => false,
        'author_or_readmore'            => 'readmore',
        'image_aspect_ratio'            => auxin_get_option( 'post_image_aspect_ratio' ),
        'desktop_cnum'                  => auxin_get_option( 'post_index_column_number' ),
        'tablet_cnum'                   => auxin_get_option( 'post_index_column_number_tablet' ),
        'phone_cnum'                    => auxin_get_option( 'post_index_column_number_mobile' ),
        'preview_mode'                  => 'grid',
        'tag'                           => '',
        'reset_query'                   => false,
        'use_wp_query'                  => true
    );

    if( function_exists( 'auxin_widget_recent_posts_callback' ) ){
        // get the shortcode base blog page
        $result = auxin_widget_recent_posts_callback( $args );
    } else {
        $result = sprintf( __('To enable this feature, please install %s plugin.', 'phlox' ), '<a href="'.admin_url('plugin-install.php?s=auxin&tab=search&type=term').'" target="_blank">' . __('Auxin Elements', 'phlox' ) . '</a>' );
    }

// if it is normal blog loop
} else {
    $q_args = '&paged='. $paged. '&posts_per_page='. get_option( 'posts_per_page' );

    // query the posts
    query_posts( $query_string . $q_args );
    // does this query has result?
    $result = have_posts();
}


// if it is not a shortcode base blog page
if( true === $result ){

    while ( have_posts() ) : the_post();
        include 'entry/post.php';
    endwhile; // end of the loop.

// if it is a shortcode base blog page
} else if( ! empty( $result ) ){
    echo $result;

// if result not found
} else {
    include 'content-none.php';
}


auxin_the_paginate_nav(
    array( 'css_class' => auxin_get_option('archive_pagination_skin') )
);
