<?php
/**
 * Template Name: Blog Archive
 *
 * 
 * @package    auxin
 * @author     averta (c) 2010-2017
 * @link       http://averta.net

 */

locate_template( 'templates/blog-main.php', true );
