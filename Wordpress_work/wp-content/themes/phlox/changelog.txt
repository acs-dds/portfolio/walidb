Version 1.6.2 / (14.01.2017)
============================
- [Improvement]: Defining API key for google maps
- [Improvement]: Improvement in animation of page loading prgoress bar
- [New]: New option for blog page to hide post info
- [New]: New social options for VK and telegram added
- [New]: New option for blog page to hide the share and tags section
- [Fix]: Fixing submenu skin and positioning for menu with submenu in top header


Version 1.6.0 / (20.12.2016)
============================
- [Fix]: Addressed a conflict in customizer with WordPress 4.7
- [Fix]: Addressed an issue with displaying the title for some shortcodes
- [New]: Implementing new option for changing the color of toolbar on mobile browser
- [Improvement]: Improvement in hover effects
- [Improvement]: In image post and image element hover effect


Version 1.5.8 / (01.12.2016)
============================
- [New]: Introducing RTL layout for Phlox theme
- [New]: Introducing grid table layout for recent grid post element
- [New]: Button for sharing the page added
- [New]: Now you can check the latest changes in phlox theme, right from your dashboard
- [New]: Plenty of new options and styles for Phlox Text element
- [New]: Carousel and tile layouts for recent posts elements added
- [New]: New layouts (Land and Tile) for blog archive page added.
- [New]: Implementing Ajax preloading and animating progress for web pages
- [New]: like button for blog posts
- [Fix]: An issue with loading the translate files from theme folder fixed
- [Fix]: An issue with tabs element fixed
- [Improvement]: Improvement in aspect ratio option for timeline element
- [Improvement]: In pagination options and styles
- [Improvement]: alignment of timeline blog layout
- [Improvement]: Display the parent theme name, if the child theme is enabled

Version 1.5.4 / (22.10.2016)
============================
- [New]: Introducing new sticky next and previous navigation in single blog posts
- [New]: Integrating & compatibility for facebook and flickr plugin
- [New]: Option for hiding column title in megamenu added
- [New]: Introducing new options and styles for narrowing down the context
- [New]: New skins for next and previous navigation in single blog posts added
- [Improvement]: Mega menu style improved
- [Improvement]: spaces on blog timeline view improved
- [Improvement]: Improvement in page load time by merging assets and under the hood improvements

Version 1.5.3 / (26.09.2016)
============================
- [New]: Introducing new content style for blog grid layout.
- [Improvement]: Extra readmore links no longer appear on archive pages.
- [Fix]: Addressing an issue that prevents to render taxonomy page properly when grid or timeline layout is active.
- [Fix]: Fixing some typos in file names

Version 1.0.0 / (21.07.2016)
============================
- Initial release
