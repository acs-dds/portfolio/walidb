<?php
/**
 * The Header for theme.
 * Displays all of the <head> section and everything up till <main id="main">
 *
 * 
 * @package    auxin
 * @author     averta (c) 2010-2017
 * @link       http://averta.net

 */
 global $page, $post, $this_page; $this_page = $post;
?>
<!DOCTYPE html>
<!--[if IE 7]>    <html class="no-js oldie ie7 ie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 8]>    <html class="no-js oldie ie8 ie" <?php language_attributes(); ?> > <![endif]-->
<!--[if IE 9 ]>   <html class="no-js       ie9 ie" <?php language_attributes(); ?> > <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--> <html class="no-js" <?php language_attributes(); ?> > <!--<![endif]-->
<head>
        <meta charset="<?php bloginfo( 'charset' ); ?>" >
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

<?php if( auxin_get_option( 'enable_site_reponsiveness', 1 ) ) { ?>
        <!-- devices setting -->
        <meta name="viewport"           content="initial-scale=1,user-scalable=no,width=device-width">
<?php } else { ?>
        <!-- devices setting -->
        <meta name="viewport"           content="initial-scale=1">
<?php } ?>

        <!-- pingback -->
        <link rel="pingback"            href="<?php bloginfo( 'pingback_url' ); ?>" />

        <!-- enables HTML5 elements & feature detects -->
        <script src="<?php echo get_template_directory_uri(); ?>/js/solo/modernizr-custom.js"></script>
        <!--Sumo account -->
        <script src="//load.sumome.com/" data-sumo-site-id="8f02803d9705c93c9151c9be30cd90721623885b4fbe6eced907df693b81b092" async="async"></script>
<!-- outputs by wp_head -->
<?php wp_head(); ?>
<!-- end wp_head -->

</head>

<body <?php body_class(); ?>>

<?php
    do_action( "auxin_after_body_open", $post );

    $inner_body_data  = '';
    $inner_body_class = '';

    if ( auxin_get_option('page_animation_nav_enable') ) {
        $inner_body_data = 'data-page-animation="true" data-page-animation-type="' . auxin_get_option('page_animation_nav_type', 'fade') . '"';
        $inner_body_class = 'aux-page-animation-' . auxin_get_option('page_animation_nav_type', 'fade') . ' ';
    }

    // insert progress bar element
    if ( auxin_get_option('page_preload_enable') && auxin_get_option('page_preload_prgoress_bar') ) {
        $progressbar_style = !empty( auxin_get_option( 'page_preload_prgoress_bar_color' ) ) ? 'style=" background-color:'.auxin_get_option( 'page_preload_prgoress_bar_color' ).';"' : '';

?>
    <div id="preloadProgressbar" class="aux-no-js" <?php echo $progressbar_style ?>></div>
<?php } ?>

<div id="inner-body" class="<?php echo $inner_body_class; ?>" <?php echo $inner_body_data ?>>

<?php
    $site_header_class  = $header_max_width = 'aux-territory aux-' . auxin_get_option('site_header_width') . '-container';
    $site_header_class .= auxin_get_option('site_header_border_bottom') ? ' aux-add-border' : '';
    $site_header_class .= 'logo-left-menu-right-over' === auxin_get_top_header_layout() ? ' aux-over-content' : '';

    if( auxin_get_option('show_topheader') ) {
?>

    <div id="top-header" class="aux-top-header <?php echo $header_max_width; ?>">
        <div class="aux-wrapper aux-float-layout">

            

        </div><!-- end wrapper -->
    </div><!-- end top header -->

<?php
    }

    do_action( 'auxin_before_the_header', $post ); ?>

    <header id="site-header" class="site-header-section <?php echo $site_header_class; ?>" data-sticky-height="<?php echo auxin_get_option('site_header_container_scaled_height','60')?>">
        <div class="aux-wrapper">
            <div class="container aux-fold">

            <?php echo auxin_get_header_layout();  ?>

            </div><!-- end of container -->
        </div><!-- end of wrapper -->
    </header><!-- end header -->

<?php do_action( 'auxin_before_the_content' );
