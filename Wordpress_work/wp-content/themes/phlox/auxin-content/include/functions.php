<?php
/**
 * Theme general functions
 *
 * 
 * @package    auxin
 * @author     averta (c) 2010-2017
 * @link       http://averta.net

 */

if( ! function_exists('auxin_famous_colors') ) {

    function auxin_get_famous_colors_list(){
        return 
            array(
            'black'    => array(
                'label'     => __('black', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-black',
            ),
            'white'    => array(
                'label'     => __('White', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-white',
            ),
            'masala'    => array(
                'label'     => __('Masala', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-masala',
            ),
            'dark-gray'    => array(
                'label'     => __('Dark Gray', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-dark-gray',
            ),
            'ball-blue'    => array(
                'label'     => __('Ball Blue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-ball-blue',
            ),
            'fountain-blue'    => array(
                'label'     => __('Fountain Blue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-fountain-blue',
            ),
            'shamrock'    => array(
                'label'     => __('Shamrock', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-shamrock',
            ),
            'curios-blue'    => array(
                'label'     => __('Curios Blue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-curios-blue',
            ),
            'light-sea-green'    => array(
                'label'     => __('Light Sea Green', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-light-sea-green',
            ),
            'emerald'    => array(
                'label'     => __('Emerald', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-emerald',
            ),
            'energy-yellow'    => array(
                'label'     => __('Energy Yellow', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-energy-yellow',
            ),
            'tulip-tree'    => array(
                'label'     => __('Tulip Tree', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tulip-tree',
            ),
            'mikado-yellow'    => array(
                'label'     => __('Mikado Yellow', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-mikado-yellow',
            ),
            'manz'    => array(
                'label'     => __('Manz', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-manz',
            ),
            'tickle-me-pink'    => array(
                'label'     => __('Tickle Me Pink', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tickle-me-pink',
            ),
            'pink-salmon'    => array(
                'label'     => __('Pink Salmon', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-pink-salmon',
            ),
            'wisteria'    => array(
                'label'     => __('Wisteria', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-wisteria',
            ),
            'lilac'    => array(
                'label'     => __('Lilac', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-lilac',
            ),
            'pale-sky'    => array(
                'label'     => __('Pale Sky', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-pale-sky',
            ),
            'tower-gray'    => array(
                'label'     => __('Tower Gray', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tower-gray',
            ),
            'silver-chalice'    => array(
                'label'     => __('Silver Chalice', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-silver-chalice',
            ),
            'bermuda'    => array(
                'label'     => __('Bermuda', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-bermuda',
            ),
            'william'    => array(
                'label'     => __('William', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-william',
            ),
            'carmine-pink'    => array(
                'label'     => __('Carmine Pink', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-carmine-pink',
            ),
            'persimmon'    => array(
                'label'     => __('Persimmon', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-persimmon',
            ),
            'tan-hide'    => array(
                'label'     => __('Tan Hide', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tan-hide',
            ),
            'flame-pea'    => array(
                'label'     => __('Flame Pea', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-flame-pea',
            ),
            'wild-watermelon'    => array(
                'label'     => __('Wild Watermelon', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-wild-watermelon',
            ),
            'iceberg'    => array(
                'label'     => __('Iceberg', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-iceberg',
            ),
            'london-hue'    => array(
                'label'     => __('London Hue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-london-hue',
            ),
            'mauve-taupe'    => array(
                'label'     => __('Mauve Taupe', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-mauve-taupe',
            ),
            'disco'    => array(
                'label'     => __('Disco', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-disco',
            ),
            'dark-lavender'    => array(
                'label'     => __('Dark Lavender', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-dark-lavender',
            ),
            'viking'    => array(
                'label'     => __('Viking', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-viking',
            ),
            'water-leaf'    => array(
                'label'     => __('Water Leaf', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-water-leaf',
            ),
            'tiffany-blue'    => array(
                'label'     => __('Tiffany Blue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tiffany-blue',
            ),
             'careys-pink'    => array(
                'label'     => __('Careys Pink', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-careys-pink',
            ),
            'tomato'    => array(
                'label'     => __('Tomato', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tomato',
            ),
            'pastel-orange'    => array(
                'label'     => __('Pastel Orange', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-pastel-orange',
            ),
            'zest'    => array(
                'label'     => __('Zest', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-zest',
            ),
            'eggplant'    => array(
                'label'     => __('Eggplant', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-eggplant',
            ),
            'east-bay'    => array(
                'label'     => __('East Bay', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-east-bay',
            ),
            'steel-blue'    => array(
                'label'     => __('Steel Blue', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-steel-blue',
            ),
            'half-backed'    => array(
                'label'     => __('Half Backed', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-half-backed',
            ),
            'nevada'    => array(
                'label'     => __('Nevada', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-nevada',
            ),
            'opal'    => array(
                'label'     => __('Opal', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-opal',
            ),
            'pine-green'    => array(
                'label'     => __('Pine Green', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-pine-green',
            ),
            'de-york'    => array(
                'label'     => __('De York', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-de-york',
            ),
            'tapestry'    => array(
                'label'     => __('Tapestry', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-tapestry',
            ),
            'fire-engine-red'    => array(
                'label'     => __('Fire Engine Red', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-fire-engine-red',
            ),
            'dark-orange'    => array(
                'label'     => __('Dark Orange', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-dark-orange',
            ),
            'brick-red'    => array(
                'label'     => __('Brick Red', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-brick-red',
            ),
            'khaki'   => array(
                'label'     => __('Khaki', 'phlox'),
                'css_class' => 'aux-color-selector aux-button aux-visual-selector-khaki',
            )
        );
 
    }

}
