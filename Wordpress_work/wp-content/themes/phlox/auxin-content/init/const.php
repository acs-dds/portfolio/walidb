<?php
/**
 * Defining Constants.
 *
 * 
 * @package    auxin
 * @author     averta (c) 2010-2017
 * @link       http://averta.net

 */

/*-----------------------------------------------------------------------------------*/
/*  Define Global Vars
/*-----------------------------------------------------------------------------------*/

// theme name
$theme_data = wp_get_theme();

// this id is used as prefix in database option field names - specific for each theme
if( ! defined('THEME_ID')        ) define( 'THEME_ID'        ,  'phlox' );
if( ! defined('THEME'.'_DOMAIN') ) define( 'THEME'.'_DOMAIN' ,  THEME_ID );

if( ! defined('THEME_NAME')      ) define( 'THEME_NAME'      , $theme_data->Name    );
if( ! defined('THEME_NAME_I18N') ) define( 'THEME_NAME_I18N' , __( 'Phlox', 'phlox' ) );


// dummy gettext call to translate theme name
__( 'Phlox', 'phlox' );
__( 'PHLOX', 'phlox' );

/*-----------------------------------------------------------------------------------*/
