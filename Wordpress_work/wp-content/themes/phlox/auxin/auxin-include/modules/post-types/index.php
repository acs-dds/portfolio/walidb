<?php
/**
 * Post types and metafields
 *
 * 
 * @package    auxin
 * @author     averta (c) 2010-2017
 * @link       http://averta.net

 */

function auxin_add_post_type_metafields(){

    $all_post_types = auxin_get_possible_post_types(true);
    $all_post_types['post'] = true;
    $all_post_types['page'] = true;

    $auxin_is_admin  = is_admin();

    foreach ( $all_post_types as $post_type => $is_post_type_allowed ) {

        if( ! $is_post_type_allowed ){
            continue;
        }

        // define metabox args
        $metabox_args = array( 'post_type' => $post_type );

        switch( $post_type ) {

            case 'page':

                $metabox_args['hub_id']        = 'axi_meta_hub_page';
                $metabox_args['hub_title']     = __('Page Options', 'phlox');
                $metabox_args['to_post_types'] = array( $post_type );

                break;

            case 'post':

                $metabox_args['hub_id']        = 'axi_meta_hub_post';
                $metabox_args['hub_title']     = __('Post Options', 'phlox');
                $metabox_args['to_post_types'] = array( $post_type );

            default:
                break;
        }

        // Load metabox fields on admin
        if( $auxin_is_admin ){
            auxin_maybe_render_metabox_hub_for_post_type( $metabox_args );
        }

    }

}

add_action( 'init', 'auxin_add_post_type_metafields' );
