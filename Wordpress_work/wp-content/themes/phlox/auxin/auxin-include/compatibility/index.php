<?php
/**
 * Compatibility for following plugind is included:
 * - WPML
 * - SEO by Yoast
 * - All in on SEO Pack
 * - Jetpack
 * - Visual Composer
 * - WooCommerce
 * - Breadcrumb NavXT
 * - Related Posts
 *
 */

// disable the automatic appending of related posts to the post content
add_filter( 'rp4wp_append_content', '__return_false' );

/*-----------------------------------------------------------------------------------*/
/*  WPML Custom Functions
/*-----------------------------------------------------------------------------------*/
require_once( 'wpml.php');

/*-----------------------------------------------------------------------------------*/
/*  Jetpack
/*-----------------------------------------------------------------------------------*/
require_once( 'jetpack.php');

/*-----------------------------------------------------------------------------------*/
/*  Jetpack
/*-----------------------------------------------------------------------------------*/
require_once( 'vc.php');

/*-----------------------------------------------------------------------------------*/
/*  WooCommerce
/*-----------------------------------------------------------------------------------*/
require_once( 'woocommerce.php');

