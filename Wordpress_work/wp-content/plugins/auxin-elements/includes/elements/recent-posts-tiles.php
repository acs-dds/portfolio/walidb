<?php
/**
 * Code highlighter element
 *
 * 
 * @package    Auxin
 * @license    LICENSE.txt
 * @author     
 * @link       http://averta.net/phlox/
 * @copyright  (c) 2010-2017 
 */

function auxin_get_recent_posts_tiles_master_array( $master_array ) {

    $categories = get_terms( 'category', 'orderby=count&hide_empty=0' );
    $categories_list = array( '' => __('All Categories', 'auxin-elements' ) )  ;
    foreach ( $categories as $key => $value ) {
        $categories_list[$value->term_id] = $value->name;
    }

    // $tags = get_terms( 'post_tag', 'orderby=count&hide_empty=0' );
    // $tags_list;
    // foreach ($tags as $key => $value) {
    //     $tags_list["$value->term_id"] = $value->name;
    // }


    $master_array['aux_recent_posts_tiles'] = array(
        'name'                          => __('[Phlox] Tiles Recent Posts', 'auxin-elements' ),
        'auxin_output_callback'         => 'auxin_widget_recent_posts_tiles_callback',
        'base'                          => 'aux_recent_posts_tiles',
        'description'                   => __('It adds recent posts in tiles style.', 'auxin-elements' ),
        'class'                         => 'aux-widget-recent-posts-tiles',
        'show_settings_on_create'       => true,
        'weight'                        => 1,
        'is_widget'                     => false,
        'is_shortcode'                  => true,
        'is_so'                         => true,
        'is_vc'                         => true,
        'category'                      => THEME_NAME,
        'group'                         => '',
        'admin_enqueue_js'              => '',
        'admin_enqueue_css'             => '',
        'front_enqueue_js'              => '',
        'front_enqueue_css'             => '',
        'icon'                          => 'auxin-element auxin-tile',
        'custom_markup'                 => '',
        'js_view'                       => '',
        'html_template'                 => '',
        'deprecated'                    => '',
        'content_element'               => '',
        'as_parent'                     => '',
        'as_child'                      => '',
        'params' => array(
            array(
                'heading'          => __('Title','auxin-elements' ),
                'description'       => __('Recent post title, leave it empty if you don`t need title.', 'auxin-elements'),
                'param_name'       => 'title',
                'type'             => 'textfield',
                'std'              => '',
                'value'            => '',
                'holder'           => 'textfield',
                'class'            => 'title',
                'admin_label'      => true,
                'dependency'       => '',
                'weight'           => '',
                'group'            => '' ,
                'edit_field_class' => ''
            ),
            array(
                'heading'           => __('Categories', 'auxin-elements'),
                'description'       => __('Specifies a category that you want to show posts from it.', 'auxin-elements' ),
                'param_name'        => 'cat',
                'type'              => 'dropdown',
                'def_value'         => '',
                'holder'            => 'dropdown',
                'class'             => 'cat',
                'value'             => $categories_list,
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Number of posts to show', 'auxin-elements'),
                'description'       => '',
                'param_name'        => 'num',
                'type'              => 'textfield',
                'def_value'         => '8',
                'holder'            => 'textfield',
                'class'             => 'num',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Exclude posts without media','auxin-elements' ),
                'description'       => '',
                'param_name'        => 'exclude_without_media',
                'type'              => 'aux_switch',
                'value'             => '1',
                'class'             => '',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Exclude custom post formats','auxin-elements' ),
                'description'       => '',
                'param_name'        => 'exclude_custom_post_formats',
                'type'              => 'aux_switch',
                'value'             => '1',
                'class'             => '',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Exclude quote and link post formats','auxin-elements' ),
                'description'       => '',
                'param_name'        => 'exclude_quote_link',
                'type'              => 'aux_switch',
                'value'             => '1',
                'class'             => '',
                'admin_label'       => true,
                'dependency'        => array(
                    'element'       => 'exclude_custom_post_formats',
                    'value'         => '0'
                ),
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Exclude posts','auxin-elements' ),
                'description'       => __('Post IDs separated by comma (eg. 53,34,87,25)', 'auxin-elements' ),
                'param_name'        => 'exclude',
                'type'              => 'textfield',
                'value'             => '',
                'holder'            => 'textfield',
                'class'             => '',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),
            array(
                'heading'           => __('Include posts','auxin-elements' ),
                'description'       => __('Post IDs separated by comma (eg. 53,34,87,25)', 'auxin-elements' ),
                'param_name'        => 'include',
                'type'              => 'textfield',
                'value'             => '',
                'holder'            => 'textfield',
                'class'             => '',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),
            array(
                'heading'            => __('Order by', 'auxin-elements'),
                'description'        => '',
                'param_name'         => 'order_by',
                'type'               => 'dropdown',
                'def_value'          => 'date',
                'holder'             => 'dropdown',
                'class'              => 'order_by',
                'value'              => array (
                    'date'            => __('Date', 'auxin-elements'),
                    'menu_order date' => __('Menu Order', 'auxin-elements'),
                    'title'           => __('Title', 'auxin-elements'),
                    'ID'              => __('ID', 'auxin-elements'),
                    'rand'            => __('Random', 'auxin-elements'),
                    'comment_count'   => __('Comments', 'auxin-elements'),
                    'modified'        => __('Date Modified', 'auxin-elements'),
                    'author'          => __('Author', 'auxin-elements'),
                ),
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),
             array(
                'heading'           => __('Order', 'auxin-elements'),
                'description'       => '',
                'param_name'        => 'order',
                'type'              => 'dropdown',
                'def_value'         => 'DESC',
                'holder'            => 'dropdown',
                'class'             => 'order',
                'value'             =>array (
                    'DESC'          => __('Descending', 'auxin-elements'),
                    'ASC'           => __('Ascending', 'auxin-elements'),
                ),
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'       => __('Start offset','auxin-elements' ),
                'description'   => __('Number of post to displace or pass over.', 'auxin-elements' ),
                'param_name'    => 'offset',
                'type'          => 'textfield',
                'value'         => '',
                'holder'        => 'textfield',
                'class'         => '',
                'admin_label'   => true,
                'dependency'    => '',
                'weight'        => '',
                'group'         => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'          => __('Post tile style','auxin-elements' ),
                'description'      => '',
                'param_name'       => 'tile_style',
                'type'             => 'aux_visual_select',
                'value'            => '',
                'holder'           => 'dropdown',
                'class'            => 'tile_style',
                'admin_label'      => true,
                'dependency'       => '',
                'weight'           => '',
                'group'            => 'Style' ,
                'edit_field_class' => '',
                'choices'          => array(
                    ''      => array(
                        'label' => __('Standard', 'auxin-elements' ),
                        'image' => AUX_URL . 'images/visual-select/button-normal.svg'
                    ),
                    'aux-overlay' => array(
                        'label' => __('Dark', 'auxin-elements' ),
                        'image' => AUX_URL . 'images/visual-select/button-curved.svg'
                    )
                )
            ),

            array(
                'heading'           => __('Insert post title','auxin-elements' ),
                'description'       => '',
                'param_name'        => 'display_title',
                'type'              => 'aux_switch',
                'value'             => '1',
                'class'             => 'display_title',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Insert post meta','auxin-elements' ),
                'description'       => '',
                'param_name'        => 'show_info',
                'type'              => 'aux_switch',
                'value'             => '1',
                'class'             => '',
                'admin_label'       => true,
                'weight'            => '',
                'group'             => '' ,
                'edit_field_class'  => ''
            ),

            array(
                'heading'           => __('Extra class name','auxin-elements' ),
                'description'       => __('If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'auxin-elements' ),
                'param_name'        => 'extra_classes',
                'type'              => 'textfield',
                'value'             => '',
                'def_value'         => '',
                'holder'            => 'textfield',
                'class'             => 'extra_classes',
                'admin_label'       => true,
                'dependency'        => '',
                'weight'            => '',
                'group'             => '',
                'edit_field_class'  => ''
            )
        )
    );

    return $master_array;
}

add_filter( 'auxin_master_array_shortcodes', 'auxin_get_recent_posts_tiles_master_array', 10, 1 );


/**
 * Element without loop and column
 * The front-end output of this element is returned by the following function
 *
 * @param  array  $atts              The array containing the parsed values from shortcode, it should be same as defined params above.
 * @param  string $shortcode_content The shorcode content
 * @return string                    The output of element markup
 */
function auxin_widget_recent_posts_tiles_callback( $atts, $shortcode_content = null ){

    global $aux_content_width;

    // Defining default attributes
    $default_atts = array(
        'title'                       => '', // header title
        'cat'                         => ' ',
        'num'                         => '4', // max generated entry
        'exclude'                     => '',
        'include'                     => '',
        'offset'                      => '',
        'paged'                       => '',
        'order_by'                    => 'menu_order date',
        'order'                       => 'desc',
        'excerpt_len'                 => '160',
        'exclude_without_media'       => 1,
        'exclude_custom_post_formats' => 1,
        'exclude_quote_link'          => 1,
        'exclude_post_formats_in'     => array(), // the list od post formats to exclude
        'tile_style'                  => '',
        'display_title'               => true,
        'show_info'                   => true,
        'extra_classes'               => '',
        'custom_el_id'                => '',
        'reset_query'                 => true,
        'use_wp_query'                => false, // true to use the global wp_query, false to use internal custom query
        'base_class'                  => 'aux-widget-recent-posts-tiles'
    );

    $result = auxin_get_widget_scafold( $atts, $default_atts, $shortcode_content );
    extract( $result['parsed_atts'] );


    // get content width
    global $aux_content_width;

    // specify the post formats that should be excluded -------
    $exclude_post_formats_in = (array) $exclude_post_formats_in;

    if( $exclude_custom_post_formats ){
        $exclude_post_formats_in = array_merge( $exclude_post_formats_in, array( 'aside', 'gallery', 'image', 'link', 'quote', 'video', 'audio' ) );
    }
    if( $exclude_quote_link ){
        $exclude_post_formats_in[] = 'quote';
        $exclude_post_formats_in[] = 'link';
    }
    $exclude_post_formats_in = array_unique( $exclude_post_formats_in );

    // --------------

    ob_start();

    global $wp_query;

    if( ! $use_wp_query ){

        // create wp_query to get latest items -----------
        $args = array(
            'post_type'             => 'post',
            'orderby'               => $order_by,
            'order'                 => $order,
            'offset'                => $offset,
            'paged'                 => $paged,
            'cat'                   => $cat,
            'post__not_in'          => array_filter( explode( ',', $exclude ) ),
            'post__in'              => array_filter( explode( ',', $include ) ),
            'post_status'           => 'publish',
            'posts_per_page'        => $num,
            'ignore_sticky_posts'   => 1
        );

        // exclude post formats if required
        if( ! empty( $exclude_post_formats_in ) ){

            // generate post-format terms (i.e post-format-aside)
            $post_format_terms = array();
            foreach ( $exclude_post_formats_in as $_post_format ) {
                $post_format_terms[] = 'post-format-' . $_post_format;
            }

            // exclude the redundant taxonomies (post-format)
            $args['tax_query'] = array(
                array(
                    'taxonomy' => 'post_format',
                    'field'    => 'slug',
                    'terms'    => $post_format_terms,
                    'operator' => 'NOT IN'
                )
            );

        }

        // whether to exclude posts without featured-image or not
        if( $exclude_without_media ){
            $args['meta_query'] = array(
                array(
                    'key'       => '_thumbnail_id',
                    'value'     => '',
                    'compare'   => '!='
                )
            );
        }

        $wp_query = new WP_Query( $args );
    }

    // widget header ------------------------------
    echo $result['widget_header'];
    echo $result['widget_title'];

    $phone_break_point  = 767;
    $tablet_break_point = 992;

    $show_comments      = true; // shows comments icon
    $post_counter       = 0;
    $item_class         = 'aux-post-tile aux-image-box';

    $container_class    = 'aux-tiles-layout ' . $tile_style;
    $column_media_width = auxin_get_content_column_width( 4, 0 );

    $have_posts = $wp_query->have_posts();

    if( $have_posts ){

        echo sprintf( '<div class="%s">', $container_class );

        while ( $wp_query->have_posts() ) {

            $wp_query->the_post();
            $post = $wp_query->post;

            $the_format = get_post_format( $post );
            $item_pattern_info = auxin_get_tile_pattern( 'default', $post_counter, $column_media_width );
            $post_counter++;

            $post_vars = auxin_get_post_format_media(
                $post,
                array(
                    'request_from'   => 'archive',
                    'media_width'    => $phone_break_point,
                    'media_size'     => 'large',
                    'upscale_image'  => true,
                    'ignore_formats' => array( '*' ),
                    'add_image_hw'   => false, // whether add width and height attr or not
                    'image_sizes'    => $item_pattern_info['image_sizes'],
                    'srcset_sizes'   => $item_pattern_info['srcset_sizes']
                )
            );

            extract( $post_vars );

            $post_classes = $item_class .' '. $item_pattern_info['classname'];

            include( locate_template( 'templates/theme-parts/entry/post-tile.php' ) );
        }

        echo '</div>';
    }

    if( $reset_query ){
        wp_reset_query();
    }

    // return false if no result found
    if( ! $have_posts ){
        ob_get_clean();
        return false;
    }

    // widget footer ------------------------------
    echo $result['widget_footer'];

    return ob_get_clean();
}
