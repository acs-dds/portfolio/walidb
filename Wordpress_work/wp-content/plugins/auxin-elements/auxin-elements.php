<?php
/**
 * Extra features, shortcodes and widgets for themes with auxin framework (i.e Phlox Theme)
 *
 * 
 * @package    Auxin
 * @license    LICENSE.txt
 * @author     
 * @link       http://averta.net/phlox/
 * @copyright  (c) 2010-2017 
 *
 * Plugin Name:       Auxin Essential Elements
 * Plugin URI:        https://wordpress.org/plugins/auxin-elements/
 * Description:       Powerful and comprehensive plugin that extends the functionality of Phlox theme by adding new shortcodes, widgets and options
 * Version:           1.3.3
 * Author:            averta
 * Author URI:        http://averta.net
 * Text Domain:       auxin-elements
 * License URI:       LICENSE.txt
 * Domain Path:       /languages
 * Tested up to: 	  4.7.1
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die('No Naughty Business Please !');
}

// Abort loading if WordPress is upgrading
if ( defined( 'WP_INSTALLING' ) && WP_INSTALLING ) {
    return;
}

/*----------------------------------------------------------------------------*/


// Run the plugin only for Phlox theme
if ( ! wp_installing() || 'wp-activate.php' === $pagenow ) {
    if ( ! file_exists( get_template_directory() . '/auxin/auxin-include/auxin.php' ) ){
        return;
    }
}


// Make sure the client has PHP version 5.3 or higher, otherwise, throw a notice
function auel_requirement_notice() {
    echo '<div class="error"><p>' . __( 'PHP version 5.3.0 or above is required for "Auxin Elements" plugin', 'auxin-elements' ) . '</p></div>';
}
if ( version_compare( PHP_VERSION, '5.3.0', '<' ) ) {
    add_action( 'admin_notices', 'auel_requirement_notice' );
    return;
}


// Check if the theme meets the plugin requirements
function auel_auxin_theme_requirement_notice() {
    if ( defined('THEME_VERSION') && version_compare( THEME_VERSION, '1.6.2', '<' ) ) {
        echo '<div class="error"><p>'.
            sprintf(
                __( 'You are using %1$s theme version %2$s. You need to update the %1$s theme to latest version in order to use %3$s plugin', 'auxin-elements' ),
                '<em>'. THEME_NAME_I18N .'</em>',
                '<em>'. THEME_VERSION . '</em>',
                '<em>'. __('Auxin Elements', 'auxin-elements') . '</em>'
            ).
            '</p></div>';
    }
}
add_action( 'admin_notices', 'auel_auxin_theme_requirement_notice' );


/*----------------------------------------------------------------------------*/

require_once( plugin_dir_path( __FILE__ ) . 'includes/define.php' 		 );
require_once( plugin_dir_path( __FILE__ ) . 'public/class-auxels.php' );

// Register hooks that are fired when the plugin is activated or deactivated.
register_activation_hook  ( __FILE__, array( 'AUXELS', 'activate'   ) );
register_deactivation_hook( __FILE__, array( 'AUXELS', 'deactivate' ) );

/*----------------------------------------------------------------------------*/
