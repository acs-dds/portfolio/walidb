<?php
/**
 * Load frontend scripts and styles
 *
 * 
 * @package    Auxin
 * @license    LICENSE.txt
 * @author     
 * @link       http://averta.net/phlox/
 * @copyright  (c) 2010-2017 
 */

/**
* Constructor
*/
class AUXELS_Frontend_Assets {


	/**
	 * Construct
	 */
	public function __construct() {

	}

    /**
     * Styles for admin
     *
     * @return void
     */
    public function load_styles() {
        //wp_enqueue_style( AUXELS_SLUG .'-main',   AUXELS_PUB_URL . '/assets/css/main.css',  array(), AUXELS_VERSION );
    }

    /**
     * Scripts for admin
     *
     * @return void
     */
    public function load_scripts() {
        //wp_enqueue_script( AUXELS_SLUG .'-main', AUXELS_PUB_URL . '/assets/js/main.js', array('jquery'), AUXELS_VERSION, true );
    }

}
return new AUXELS_Frontend_Assets();
