<?php // load admin related classes & functions

// load admin related functions
include_once( 'admin-the-functions.php' );

// load admin related classes
include_once( 'classes/class-auxels-admin-assets.php'  );
include_once( 'classes/class-auxels-import.php' );

do_action( 'auxels_admin_classes_loaded' );

// load admin related functions
include_once( 'admin-hooks.php' );
