<?php
/**
 * Add Video Option meta box for video post format
 *
 * 
 * @package    Auxin
 * @license    LICENSE.txt
 * @author     
 * @link       http://averta.net/phlox/
 * @copyright  (c) 2010-2017 
*/

// no direct access allowed
if ( ! defined('ABSPATH') )  exit;


/*======================================================================*/

function auxin_metabox_fields_post_video(){

    $model            = new Auxin_Metabox_Model();
    $model->id        = 'post-video';
    $model->title     = __('Video Post options', THEME_DOMAIN);
    $model->css_class = 'aux-format-tab';
    $model->fields    = array(

        array(
            'title'         => __('Video file', THEME_DOMAIN),
            'description'   => __('Please upload an MP4/OGG/WEBM file for self hosted video player.', THEME_DOMAIN),
            'id'            => '_format_video_attachment',
            'type'          => 'video',
            'default'       => ''
        ),
        array(
            'title'         => __('Poster image', THEME_DOMAIN),
            'description'   => __('Please specify an image as a poster for self hosted video player.', THEME_DOMAIN),
            'id'            => '_format_video_attachment_poster',
            'type'          => 'image',
            'default'       => ''
        ),
        array(
            'title'         => __('Video URL (oEmbed)', THEME_DOMAIN),
            'description'   => __('Youtube or vimeo link', THEME_DOMAIN),
            'id'            => '_format_video_embed',
            'type'          => 'textarea',
            'default'       => ''
        ),
        array(
            'title'         => __('Player Skin', THEME_DOMAIN),
            'description'   => __('Specifies the skin for audio player.', THEME_DOMAIN),
            'id'            => '_format_video_player_skin',
            'type'          => 'radio-image',
            'default'       => 'default',
            'choices' => array(
                'default' => array(
                    'label'  => __('Default (set in theme options)', THEME_DOMAIN),
                    'image' => AUX_URL . 'images/visual-select/default2.svg'
                ),
                'light' => array(
                    'label'  => __('Light', THEME_DOMAIN),
                    'image' => AUX_URL . 'images/visual-select/audio-player-light.svg'
                ),
                'dark' => array(
                    'label'  => __('Dark', THEME_DOMAIN),
                    'image' => AUX_URL . 'images/visual-select/audio-player-dark.svg'
                )
            )
        )
    );

    return $model;
}
