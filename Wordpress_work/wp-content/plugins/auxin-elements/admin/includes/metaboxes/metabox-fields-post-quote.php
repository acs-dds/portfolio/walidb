<?php
/**
 * Add quote Option meta box for quote post format
 *
 * 
 * @package    Auxin
 * @license    LICENSE.txt
 * @author     
 * @link       http://averta.net/phlox/
 * @copyright  (c) 2010-2017 
*/

// no direct access allowed
if ( ! defined('ABSPATH') )  exit;


/*======================================================================*/

function auxin_metabox_fields_post_quote(){

    $model            = new Auxin_Metabox_Model();
    $model->id        = 'post-quote';
    $model->title     = __('Quote Post options', THEME_DOMAIN);
    $model->css_class = 'aux-format-tab';
    $model->fields    = array(

        array(
            'title'         => __('Source Name', THEME_DOMAIN),
            'description'   => __('The Source name', THEME_DOMAIN),
            'id'            => '_format_quote_source_name',
            'type'          => 'text',
            'default'       => ''
        ),
        array(
            'title'         => __('Source URL', THEME_DOMAIN),
            'description'   => __('Add the URL', THEME_DOMAIN),
            'id'            => '_format_quote_source_url',
            'type'          => 'text',
            'default'       => ''
        )

    );

    return $model;
}
